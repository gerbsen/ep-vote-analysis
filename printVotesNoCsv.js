const fs = require('fs');
const _ = require('lodash');
const Polls = require('./polls');
const markdownTable = require('markdown-table')

const constituencies = [
    {
        name: "Sachsen",
        persons: ["Giegold", "Winkler","Jahr","Krehl","Ernst"]
    },
    {
        name: "Sachsen-Anhalt",
        persons: ["Giegold", "Lietz", "Schulze"]
    },
    {
        name: "Thüringen",
        persons: ["Giegold", "Koch", "Jakob", "Zimmer"]
    },
    {
        name: "Baden-Würtenberg",
        persons: ["Caspary", "Gebhardt", "Gräßle", "Heubuch", "Kölmel", "Lins", "Meuthen", "Schwab", "Simon", "Wieland"]
    },
    {
        name: "Niedersachsen",
        persons: ["Gehrold", "Gieseke", "Harms", "Bernd Lange", "Lösing", "Lucke", "McAllister", "Meißner", "Quisthoudt-Rowohl", "Woelken"]
    },
    {
        name: "Rheinland-Pfalz",
        persons: ["Collin-Langen", "Detjen", "Franz", "Langen", "Neuser"]
    },
    {
        name: "Saarland",
        persons: ["Giegold", "Eck", "Leinen"]
    },
    {
        name: "Schleswig-Holstein",
        persons: ["Giegold", "Böge", "Rodust", "Trebesius"]
    },
    {
        name: "Mecklenburg-Vorpommern",
        persons: ["Bütikofer", "Gericke", "Hoffmann", "Kuhn"]
    },
    {
        name: "Hamburg",
        persons: ["Giegold", "Fleckenstein"]
    },
    {
        name: "Berlin",
        persons: ["Cramer", "Henkel", "Kaufmann", "Michels", "Sonneborn", "Voigt", "Zeller"]
    },
    {
        name: "Bayern",
        persons: ["Buchner", "Deß", "Ertug", "Ferber", "Händel", "Hirsch", "Hohlmeier", "Lochbihler", "Müller", "Niebler", "Noichl", "Starbatty", "Weber", "Westphal"]
    },
    {
        name: "Hessen",
        persons: ["Bullmann", "Gahler", "Häusling", "Klinz", "Mann", "Reda", "Werner Kuhn"]
    },
    {
        name: "Bremen",
        persons: ["Giegold", "Schuster", "Trüpel"]
    },
    {
        name: "Brandenburg",
        persons: ["Ehler", "Keller", "Melior", "Scholz"]
    },
    {
        name: "Nordrhein-Westfalen",
        persons: ["Elmar", "Florenz", "Geier", "Giegold", "Kammerevert", "Kohn", "Köster", "Liese", "Pieper", "Pretzell", "Preuß", "Radtke", "Reintke", "Schirdewan", "Sippel", "Sommer", "Verheyen", "Voss"]
    }
]
var allPersons = [];
var matchedPerson = [];
constituencies.forEach(function(constituency){

    const filename = `results/${constituency.name}.md`
    fs.writeFileSync(filename, '# ' + constituency.name + '\n');
    fs.appendFileSync(filename, 'Dieses Dokument wurde erstellt von Daniel Gerber. Für Änderungen und Erweiterungen bitte bei daniel.gerber@gruene-sachsen.de melden. Diese Analyse wurde erst durch die Arbeit von apgeordnetenwatch.de möglich gemacht.');
    _.sortBy(Polls.polls, 'title').forEach(function(poll){

        fs.appendFileSync(filename, '\n## ' + poll.title + '\n\n');
        fs.appendFileSync(filename, poll.summary.replace("\r\n\r\n\r\n\r\n\t", "\n\n") + '\n\n');
        fs.appendFileSync(filename, (poll.meta.topics.length>0?"- ":"") + poll.meta.topics.join("\n- ") + '\n\n');

        var table = [];
        table.push(['Name', 'Partei', 'Dafür gestimmt', 'Enthalten', 'Degegen gestimmt', 'Nicht beteiligt']);

        var selectedVotes = [];
        constituency.persons.forEach(function(person){
            poll.votes.forEach(function(vote){

                allPersons.push(person);
                if ( vote.fullname.indexOf(person) >= 0 ) {

                    selectedVotes.push(vote);
                    matchedPerson.push(person);
                }
            });
        })

        selectedVotes.forEach(voteByPerson => {

            var line = [voteByPerson.fullname, voteByPerson.party,
                voteByPerson.vote == "dafür gestimmt" ? "x" : '',
                voteByPerson.vote == "enthalten"  ? "x": '',
                voteByPerson.vote == "dagegen gestimmt" ? "x" : '',
                voteByPerson.vote == "nicht beteiligt" ? "x" : ''];
            table.push(line);
        });

        if ( poll.stats ) {

            ["total", "AfD", "CDU", "CSU", "Die Blauen", "DIE GRÜNEN", "DIE LINKE", "FDP", "SPD"].forEach(partei => {

                if ( poll.stats[partei] ) {

                    var line = ['', partei.replace("total", "Total"),
                        poll.stats[partei]["dafür gestimmt"] || '0',
                        poll.stats[partei]["enthalten"] || '0',
                        poll.stats[partei]["dagegen gestimmt"] || '0',
                        poll.stats[partei]["nicht beteiligt"] || '0'];
                    table.push(line);
                }
            });
        }
        fs.appendFileSync(filename, markdownTable(table) + "\n\n");
        fs.appendFileSync(filename, 'Details: [abgeordnetenwatch.de]('+poll.meta.url+')\n');
        fs.appendFileSync(filename, "<div style=\"page-break-after: always;\"></div>\n");
    })
})

// console.log(new Set(matchedPerson))
// console.log(new Set(allPersons))
// console.log(new Set(_.difference(allPersons, matchedPerson)));
