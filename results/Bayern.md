# Bayern
Dieses Dokument wurde erstellt von Daniel Gerber. Für Änderungen und Erweiterungen bitte bei daniel.gerber@gruene-sachsen.de melden. Diese Analyse wurde erst durch die Arbeit von apgeordnetenwatch.de möglich gemacht.
##  Agenda für die Zukunft der Weltmeere

Die Entschließung des Europäischen Parlaments fordert die Mitgliedstaaten eindringlich auf, umfassende Maßnahmen zum Schutz der Weltmeere zu ergreifen. Bei 83 (12%) Enthaltungen und 25 Gegenstimmen (4%) stimmten 558 (84%) Parlamentarier*innen mit "JA".

- Umwelt

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                | x         |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           | x                |                 |
| Maria Noichl                | SPD                           |                |           |                  | x               |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                | x         |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 76             | 5         | 5                | 10              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 26             | 0         | 1                | 2               |
|                             | CSU                           | 3              | 1         | 1                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 10             | 0         | 1                | 0               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 2              | 0         | 0                | 1               |
|                             | SPD                           | 22             | 0         | 0                | 5               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/agenda-fuer-die-zukunft-der-weltmeere)
<div style="page-break-after: always;"></div>

##  EU-Urheberrechtsreform

Das EU-Parlament hat am 5. Juli über eine Urheberrechtsreform abgestimmt. Mit 318 (53 %) zu 278 (47 %) Stimmen wurde die Reform abgelehnt. Zu einer erneuten Abstimmung über die Änderungsanträge wird es voraussichtlich im September kommen. Von den 96 deutschen EU-Abgeordneten stimmten 36 dafür, 49 dagegen, 11 waren nicht beteiligt. Die CDU/CSU-Abgeordneten stimmten geschlossen für die Reform,&nbsp;die SPD-, FDP- sowie Linken-Abgeordneten geschlossen dagegen. Die anderen Fraktionen&nbsp;waren geteilter Meinung.

- Medien, Kommunikation und Informationstechnik
- Recht

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           | x                |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           |                |           | x                |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           |                |           | x                |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                |           | x                |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           | x                |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           |                |           | x                |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           |                |           | x                |                 |
|                             | Total                         | 36             | 0         | 49               | 10              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 25             | 0         | 0                | 4               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 3              | 0         | 5                | 2               |
|                             | DIE LINKE                     | 0              | 0         | 5                | 2               |
|                             | FDP                           | 0              | 0         | 3                | 0               |
|                             | SPD                           | 0              | 0         | 25               | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/eu-urheberrechtsreform)
<div style="page-break-after: always;"></div>

## Anbau von gentechnisch veränderten Sojabohnen in der Europäischen Union

Das Europäische Parlament hat über die Zulassung von gentechnisch verändertem Soja abgestimmt und die Kommission aufgerufen, ihren Vorschlag erneut zu überarbeiten. Mit den Stimmen von Sozialdemokraten, Grünen und Linken wurde die EU-Kommission aufgefordert die bisher geltende Regelung, nach der jeder Mitgliedsstaat entscheiden kann, ob er gentechnisch verändertes Soja anbaut, auszusetzen.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Landwirtschaft und Ernährung

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           | x                |                 |
| Thomas Händel               | DIE LINKE                     | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           |                |           | x                |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                | x         |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           | x                |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           |                |           | x                |                 |
|                             | Total                         | 47             | 4         | 36               | 8               |
|                             | AfD                           | 1              | 0         | 0                | 0               |
|                             | CDU                           | 0              | 3         | 24               | 2               |
|                             | CSU                           | 0              | 0         | 5                | 0               |
|                             | Die Blauen                    | 0              | 0         | 0                | 1               |
|                             | DIE GRÜNEN                    | 11             | 0         | 0                | 0               |
|                             | DIE LINKE                     | 6              | 0         | 0                | 1               |
|                             | FDP                           | 0              | 0         | 2                | 1               |
|                             | SPD                           | 24             | 0         | 0                | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/anbau-von-gentechnisch-veraenderten-sojabohnen-der-europaeischen-union)
<div style="page-break-after: always;"></div>

## Anerkennung Palästinas

Das EU-Parlament hat sich für eine Anerkennung Palästinas als eigener Staat ausgesprochen - allerdings unter der Voraussetzung, dass es zu erfolgreichen Friedensgesprächen zwischen Israel und Palästinas kommt.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Außenpolitik und internationale Beziehungen
- Europapolitik und Europäische Union

| Name                    | Partei       | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| ----------------------- | ------------ | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner | ÖDP          | x              |           |                  |                 |
| Albert Deß              | CSU          | x              |           |                  |                 |
| Ismail Ertug            | SPD          | x              |           |                  |                 |
| Markus Ferber           | CSU          | x              |           |                  |                 |
| Thomas Händel           | DIE LINKE    |                |           |                  | x               |
| Monika Hohlmeier        | CSU          | x              |           |                  |                 |
| Barbara Lochbihler      | DIE GRÜNEN   | x              |           |                  |                 |
| Ulrike Müller           | FREIE WÄHLER | x              |           |                  |                 |
| Dr. Angelika Niebler    | CSU          | x              |           |                  |                 |
| Maria Noichl            | SPD          | x              |           |                  |                 |
| Manfred Weber           | CSU          | x              |           |                  |                 |
| Kerstin Westphal        | SPD          | x              |           |                  |                 |
|                         | Total        | 76             | 5         | 4                | 6               |
|                         | AfD          | 0              | 1         | 1                | 0               |
|                         | CDU          | 27             | 0         | 1                | 1               |
|                         | CSU          | 5              | 0         | 0                | 0               |
|                         | DIE GRÜNEN   | 11             | 0         | 0                | 0               |
|                         | DIE LINKE    | 6              | 0         | 0                | 1               |
|                         | FDP          | 0              | 3         | 0                | 0               |
|                         | SPD          | 23             | 1         | 0                | 3               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/anerkennung-palaestinas)
<div style="page-break-after: always;"></div>

## Angemessenheit des Datenschutzniveaus in Japan 

Am 13.12.2018 stimmte das Europäische Parlament über einen Entschließungsantrag zu einer Erklärung der Kommission über die "Angemessenheit des von Japan gewährten Schutzes personenbezogener Daten" ab. Im Rahmen des Freihandelsabkommens zwischen der EU und Japan hatten sich die beiden Partner außerdem auf die gegenseitige Anerkennung eines gleichwertigen Datenschutzniveaus geeinigt, weshalb nun im Parlament über diesen Angemessenheitsbeschluss abgestimmt wurde. Durch diesen Beschluss sollen die Daten zwischen der EU und Japan einfacher fließen.

Mit 93% Zustimmungen wurde der Antrag angenommen. Nun&nbsp;liegt es bei der Kommission, den Beschluss abschließend zu erlassen.

- Außenpolitik und internationale Beziehungen
- Medien, Kommunikation und Informationstechnik
- Recht

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           |                  | x               |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           |                  | x               |
| Thomas Händel               | DIE LINKE                     | x              |           |                  |                 |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           |                  | x               |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           |                |           |                  | x               |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 73             | 1         | 1                | 21              |
|                             | AfD                           | 0              | 1         | 0                | 0               |
|                             | CDU                           | 25             | 0         | 0                | 4               |
|                             | CSU                           | 3              | 0         | 0                | 2               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 10             | 0         | 0                | 1               |
|                             | DIE LINKE                     | 4              | 0         | 0                | 3               |
|                             | FDP                           | 2              | 0         | 0                | 1               |
|                             | SPD                           | 20             | 0         | 0                | 7               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/angemessenheit-des-datenschutzniveaus-japan)
<div style="page-break-after: always;"></div>

## Antrag zur Bekämpfung der Jugendarbeitslosigkeit

Das Europäische Parlament hat die Kommission und den Ministerrat in einem Entschließungsantrag zur Bekämpfung der Jugendarbeitslosigkeit in Europa aufgefordert.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Arbeit und Beschäftigung
- Europapolitik und Europäische Union
- Gesellschaftspolitik, soziale Gruppen
- Jugend

| Name                    | Partei       | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| ----------------------- | ------------ | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner | ÖDP          | x              |           |                  |                 |
| Albert Deß              | CSU          | x              |           |                  |                 |
| Ismail Ertug            | SPD          | x              |           |                  |                 |
| Markus Ferber           | CSU          | x              |           |                  |                 |
| Thomas Händel           | DIE LINKE    |                |           |                  | x               |
| Monika Hohlmeier        | CSU          | x              |           |                  |                 |
| Barbara Lochbihler      | DIE GRÜNEN   | x              |           |                  |                 |
| Ulrike Müller           | FREIE WÄHLER | x              |           |                  |                 |
| Dr. Angelika Niebler    | CSU          | x              |           |                  |                 |
| Maria Noichl            | SPD          | x              |           |                  |                 |
| Manfred Weber           | CSU          |                |           |                  | x               |
| Kerstin Westphal        | SPD          | x              |           |                  |                 |
|                         | Total        | 66             | 2         | 6                | 17              |
|                         | AfD          | 0              | 0         | 1                | 1               |
|                         | CDU          | 20             | 0         | 1                | 8               |
|                         | CSU          | 4              | 0         | 0                | 1               |
|                         | DIE GRÜNEN   | 10             | 0         | 0                | 1               |
|                         | DIE LINKE    | 2              | 2         | 1                | 2               |
|                         | FDP          | 3              | 0         | 0                | 0               |
|                         | SPD          | 24             | 0         | 0                | 3               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/antrag-zur-bekaempfung-der-jugendarbeitslosigkeit)
<div style="page-break-after: always;"></div>

## Assoziierungsabkommen zwischen EU und Ukraine - Wie geht es weiter?

Die Kommission wurde am 12.12.2018 in einem Entschließungsantrag des EP aufgefordert, die weiteren Reformprozesse in der Ukraine zu beobachten und schriftlich Jahresberichte zu erstellen, welche dem Parlament und dem Rat vorgelegt werden müssen.Der Antrag wurde mit 76%&nbsp; Zustimmungen angenommen. Von den deutschen MdEP stimmten vor allem die Sozial- und die Christdemokraten für den Antrag, auch die Grünen stimmten dafür. Dagegen stimmten hauptsächlich Mitglieder der Vereinten Europäischen Linken/Nordischen Grünen Linken - Fraktion.

- Außenpolitik und internationale Beziehungen
- Parlamentsangelegenheiten
- Politisches Leben, Parteien

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           |                  | x               |
| Albert Deß                  | CSU                           |                |           |                  | x               |
| Ismail Ertug                | SPD                           |                |           |                  | x               |
| Markus Ferber               | CSU                           |                |           |                  | x               |
| Thomas Händel               | DIE LINKE                     |                |           | x                |                 |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           |                  | x               |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           |                |           |                  | x               |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 60             | 2         | 9                | 25              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 21             | 1         | 0                | 7               |
|                             | CSU                           | 1              | 0         | 0                | 4               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 11             | 0         | 0                | 0               |
|                             | DIE LINKE                     | 0              | 0         | 5                | 2               |
|                             | FDP                           | 2              | 0         | 0                | 1               |
|                             | SPD                           | 17             | 1         | 0                | 9               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/assoziierungsabkommen-zwischen-eu-und-ukraine-wie-geht-es-weiter)
<div style="page-break-after: always;"></div>

## Aufbau der Infrastruktur für alternative Kraftstoffe in der EU

Am 01.10.2018 wurde über die vom Europäischen Parlament vorgelegte Resolution zum "Aufbau der Infrastruktur alternativer Kraftstoffe in der EU" abgestimmt. Darin fordert das EP eine dahingehende Ergänzung der schon bestehenden Richtlinie aus dem Jahr 2014, dass die Mitgliedstaaten verbindliche Vorgaben für die Umstellung auf alternative Kraftstoffe bekommen sollen.

Mit 215 Ja-Stimmen (72%) wurde der Antrag angenommen, da nur 298 MdEP an der Abstimmung teilnahmen. Von den 96 deutschen Abgeordneten nahmen 41 teil, davon stimmten 32 MdEP zu und 7 dagegen. Die Zustimmungen stammen vor allem von den Christ- und Sozialdemokraten, die Gegenstimmen kommen auch von den Grünen, Piraten und der ÖDP sowie von der AfD und ALFA.

- Europapolitik und Europäische Union
- Umwelt
- Wirtschaft

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           | x                |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           |                  | x               |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           |                |           |                  | x               |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                |           |                  | x               |
| Ulrike Müller               | FREIE WÄHLER                  |                |           |                  | x               |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           |                  | x               |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 32             | 2         | 7                | 55              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 15             | 0         | 0                | 14              |
|                             | CSU                           | 4              | 0         | 0                | 1               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 0              | 0         | 2                | 9               |
|                             | DIE LINKE                     | 1              | 0         | 0                | 6               |
|                             | FDP                           | 1              | 0         | 0                | 2               |
|                             | SPD                           | 10             | 0         | 0                | 17              |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/aufbau-der-infrastruktur-fuer-alternative-kraftstoffe-der-eu)
<div style="page-break-after: always;"></div>

## Ausweitung des digitalen Binnenmarkts

Das Europäische Parlament hat sich mit dieser Resolution u.a. für eine Stärkung von Verbrauerrechten im digitalen Binnenmarkt ausgesprochen.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Medien, Kommunikation und Informationstechnik
- Wirtschaft

| Name                    | Partei       | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| ----------------------- | ------------ | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner | ÖDP          |                |           | x                |                 |
| Albert Deß              | CSU          | x              |           |                  |                 |
| Ismail Ertug            | SPD          | x              |           |                  |                 |
| Markus Ferber           | CSU          | x              |           |                  |                 |
| Thomas Händel           | DIE LINKE    |                |           |                  | x               |
| Monika Hohlmeier        | CSU          | x              |           |                  |                 |
| Barbara Lochbihler      | DIE GRÜNEN   |                |           | x                |                 |
| Ulrike Müller           | FREIE WÄHLER |                |           | x                |                 |
| Dr. Angelika Niebler    | CSU          | x              |           |                  |                 |
| Maria Noichl            | SPD          | x              |           |                  |                 |
| Manfred Weber           | CSU          | x              |           |                  |                 |
| Kerstin Westphal        | SPD          |                |           |                  | x               |
|                         | Total        | 50             | 6         | 14               | 21              |
|                         | AfD          | 2              | 0         | 0                | 0               |
|                         | CDU          | 24             | 0         | 0                | 5               |
|                         | CSU          | 5              | 0         | 0                | 0               |
|                         | DIE GRÜNEN   | 0              | 0         | 7                | 4               |
|                         | DIE LINKE    | 0              | 5         | 0                | 2               |
|                         | FDP          | 0              | 0         | 1                | 2               |
|                         | SPD          | 16             | 1         | 2                | 8               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/ausweitung-des-digitalen-binnenmarkts)
<div style="page-break-after: always;"></div>

## Auszahlung von Griechenlandhilfen

Das Europäische Parlament hat der zügigen Umsetzung des Wachstums- und Beschäftigungsplans für Griechenland zugestimmt.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Außenpolitik und internationale Beziehungen
- Europapolitik und Europäische Union
- Öffentliche Finanzen, Steuern und Abgaben

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           | x                |                 |
| Thomas Händel               | DIE LINKE                     | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           |                | x         |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           | x                |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 63             | 8         | 16               | 8               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 14             | 4         | 7                | 4               |
|                             | CSU                           | 2              | 1         | 2                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 10             | 0         | 0                | 1               |
|                             | DIE LINKE                     | 6              | 0         | 0                | 1               |
|                             | FDP                           | 0              | 3         | 0                | 0               |
|                             | SPD                           | 25             | 0         | 0                | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/auszahlung-von-griechenlandhilfen)
<div style="page-break-after: always;"></div>

## Autonomes Fahren im europäischen Verkehrswesen 

Die Kommission und die Mitgliedstaaten werden im&nbsp;Entschließungsantrag aufgefordert, ihre Strategien für autonomes Fahren umfassender zu gestalten, sodass sie auch öffentliche Verkehrsmittel beinhalten.

Unter namentlicher Abstimmung wurde der Antrag des Parlaments mit 85% Zustimmung angenommen. Die deutschen Abgeordneten stimmten fast ausschließlich zu, nur vier Abgeordnete waren dagegen oder enthielten sich. Die Gegenstimmen kamen aus&nbsp;rechtspopulistischen bis rechtsextremen Fraktionen.

- Raumordnung, Bau- und Wohnungswesen
- Verkehr
- Wissenschaft, Forschung und Technologie

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     | x              |           |                  |                 |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 88             | 1         | 3                | 3               |
|                             | AfD                           | 0              | 1         | 0                | 0               |
|                             | CDU                           | 29             | 0         | 0                | 0               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 10             | 0         | 0                | 1               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 26             | 0         | 0                | 0               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/autonomes-fahren-im-europaeischen-verkehrswesen)
<div style="page-break-after: always;"></div>

## Bekämpfung der Terrorismusfinanzierung

Das Europäische Parlament richtet sich mit einer Empfehlung an den Europäischen Rat, die EU-Kommission und die Hohe Vertreterin der Union für Außen- und Sicherheitspolitik. Ziel soll es sein, internationale Terrorfinanzierung zu unterbinden. Die Empfehlung wurde mit 533 Ja-Stimmen bei 24 Gegenstimmen und 43 Enthaltungen angenommen. Christdemokraten, Sozialdemokraten, Liberale, Linke und GRÜNE stimmten für die Entschließung. Vereinzelte Nein-Stimmen kamen aus den nationalistischen und rechtsextremistischen Fraktionen.

Von den 96 deutschen EU-Abgeordneten stimmten 67 für die Entschließung, 22 waren nicht beteiligt und 7 enthielten sich ihrer Stimme. Gegenstimmen gab es keine.

- Außenpolitik und internationale Beziehungen
- Innere Sicherheit
- Verteidigung

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           |                |           |                  | x               |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     | x              |           |                  |                 |
| Nadja Hirsch                | FDP                           |                |           |                  | x               |
| Monika Hohlmeier            | CSU                           |                |           |                  | x               |
| Barbara Lochbihler          | DIE GRÜNEN                    |                |           |                  | x               |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 68             | 7         | 0                | 21              |
|                             | AfD                           | 1              | 0         | 0                | 0               |
|                             | CDU                           | 22             | 0         | 0                | 7               |
|                             | CSU                           | 4              | 0         | 0                | 1               |
|                             | Die Blauen                    | 0              | 0         | 0                | 1               |
|                             | DIE GRÜNEN                    | 7              | 0         | 0                | 4               |
|                             | DIE LINKE                     | 1              | 6         | 0                | 0               |
|                             | FDP                           | 2              | 0         | 0                | 1               |
|                             | SPD                           | 22             | 0         | 0                | 5               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/bekaempfung-der-terrorismusfinanzierung)
<div style="page-break-after: always;"></div>

## Beschleunigte Beilegung von Handelsstreitigkeiten

Der Antrag fordert die Kommission auf, die Möglichkeit eines europäischen Handelsgerichtes näher zu prüfen. Es solle die Gerichte der EU-Mitgliedsstaaten ergänzen und europäische Zivilverfahren beschleunigen sowie&nbsp;auch ein zusätzliches internationales Forum bieten, welches seinen Fokus auf die Beilegung von Handelsstreitigkeiten legen solle.&nbsp;

Die deutschen EU-Abgeordneten stimmten mehrheitlich für den Antrag. Lediglich zwei Stimmen enthielten sich. Somit wurde die Entschließung angenommen.

- Außenwirtschaft

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           |                  | x               |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           |                  | x               |
| Thomas Händel               | DIE LINKE                     | x              |           |                  |                 |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           |                  | x               |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           |                |           |                  | x               |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 73             | 2         | 0                | 21              |
|                             | AfD                           | 0              | 1         | 0                | 0               |
|                             | CDU                           | 25             | 0         | 0                | 4               |
|                             | CSU                           | 3              | 0         | 0                | 2               |
|                             | Die Blauen                    | 0              | 1         | 0                | 0               |
|                             | DIE GRÜNEN                    | 10             | 0         | 0                | 1               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 2              | 0         | 0                | 1               |
|                             | SPD                           | 19             | 0         | 0                | 8               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/beschleunigte-beilegung-von-handelsstreitigkeiten)
<div style="page-break-after: always;"></div>

## Cyber Abwehr

Die Entschließung des Europäischen Parlaments&nbsp;fordert alle Mitgliedsstaaten auf, ihre nationalen Strategien für die Cybersicherheit auf den Schutz der Informationssysteme und der damit verbundenen Daten auszurichten und den Schutz dieser kritischen Infrastruktur als Teil ihrer jeweiligen Sorgfaltspflicht zu betrachten. Mit 476 Ja-Stimmen (72%)&nbsp;wurde die Entschließung angenommen. 151 (23%) Parlamentarier*innen stimmten mit "Nein", während sich 36 (5%) enthielten. Christdemokraten, Sozialdemokraten und Liberale stimmten für die Entschließung. Grüne und Linke stimmten dagegen. Bei den Nationalisten und Rechtsextremisten zeichnete sich ein uneinheitliches Bild ab.

Von den 96 deutschen EU-Abgeordneten stimmten 54 für die Entschließung, 25 dagegen, 14 waren nicht beteiligt und 3 enthielten sich ihrer Stimme. Das Parlament&nbsp;beauftragt seinen Präsidenten, diese Entschließung dem Rat und der Kommission zu übermitteln.

- Verteidigung

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           | x                |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           |                |           |                  | x               |
| Markus Ferber               | CSU                           |                |           |                  | x               |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                |           | x                |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 54             | 3         | 24               | 15              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 28             | 1         | 0                | 0               |
|                             | CSU                           | 4              | 0         | 0                | 1               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 0              | 2         | 8                | 1               |
|                             | DIE LINKE                     | 0              | 0         | 6                | 1               |
|                             | FDP                           | 2              | 0         | 0                | 1               |
|                             | SPD                           | 16             | 0         | 1                | 10              |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/cyber-abwehr)
<div style="page-break-after: always;"></div>

## Die Rolle der Städte im institutionellen Gefüge der EU

Die Entschließung des Europäischen Parlaments fordert eine verstärkte Zusammenarbeit zwischen dem Europäischen Rat und den lokalen Gebietskörperschaften. Mit 499 Ja-Stimmen (74%)&nbsp;wurde die Entschließung angenommen und das Parlament&nbsp;sprach sich dafür aus, diese Entschließung dem Rat und der Kommission zu übermitteln. 70 (10%) Parlamentarier*innen stimmten mit "Nein", während sich 102 (15%) enthielten. Christdemokraten, Sozialdemokraten, Grüne und Linke stimmten für die Entschließung. Die&nbsp;Liberalen enthielten sich in den meisten Fällen. Die nationalistischen und rechtsextremistischen Fraktionen konnten keine gemeinsame Position finden, stimmten aber oft mit "Nein" oder enthielten sich.

Von den 96 deutschen EU-Abgeordneten stimmten 78 für die Entschließung, 8 dagegen, 5 waren nicht beteiligt und 5 enthielten sich ihrer Stimme.

- Raumordnung, Bau- und Wohnungswesen
- Staat und Verwaltung

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           |                | x         |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                | x         |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 78             | 5         | 8                | 5               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 27             | 1         | 0                | 1               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 10             | 0         | 0                | 1               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 0              | 3         | 0                | 0               |
|                             | SPD                           | 26             | 0         | 0                | 1               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/die-rolle-der-staedte-im-institutionellen-gefuege-der-eu)
<div style="page-break-after: always;"></div>

## Digitales Europa

Am 13.12.2018 stimmten die EU-Abgeordneten über einen legislativen Entschließungsantrag des Europäischen Parlaments und des Rates über das Programm "Digitales Europa" ab. Die deutschen EU-Abgeordneten stimmten mehrheitlich für den Entschließungsantrag. Der Antrag wurde angenommen.

- Europapolitik und Europäische Union
- Digitale Agenda

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           |                  | x               |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           |                  | x               |
| Thomas Händel               | DIE LINKE                     | x              |           |                  |                 |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           |                  | x               |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           |                |           |                  | x               |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 73             | 1         | 4                | 18              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 25             | 0         | 0                | 4               |
|                             | CSU                           | 3              | 0         | 0                | 2               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 10             | 0         | 0                | 1               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 2              | 0         | 0                | 1               |
|                             | SPD                           | 22             | 0         | 0                | 5               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/digitales-europa)
<div style="page-break-after: always;"></div>

## EU-Beitrittsverhandlungen mit der Türkei einfrieren

Vor dem Hintergrund der seit dem gescheiterten Militärputsch im Juli 2016 immer repressiver werdenden Politik der türkischen Regierung unter Recep Tayyip Erdoğan hat das EU-Parlament das vorübergehende Aussetzen der EU-Beitrittsverhandlungen mit der Türkei gefordert.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           |                  | x               |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 74             | 4         | 5                | 12              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 22             | 2         | 2                | 3               |
|                             | CSU                           | 4              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 0                | 1               |
|                             | DIE GRÜNEN                    | 10             | 0         | 1                | 0               |
|                             | DIE LINKE                     | 4              | 2         | 0                | 1               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 22             | 0         | 0                | 5               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/eu-beitrittsverhandlungen-mit-der-tuerkei-einfrieren)
<div style="page-break-after: always;"></div>

## EU-Japan Freihandelsabkommen (JEFTA)

Seit 2013 laufen die Verhandlungen zu einem Freihandelsabkommen zwischen der Europäischen Union und Japan, dem sogenannten "Japan-EU Free Trade Agreement (JEFTA)". Im Juli 2018 unterzeichneten Kommissionspräsident Jean-Claude Juncker, Ratspräsident Donald Tusk und der japanische Regierungschef Shinzo Abe das Freihandelsabkommen, im Dezember 2018 wurde abschließend im Europäischen Parlament abgestimmt. Durch JEFTA sollen 99 Prozent der Zölle zwischen Japan und der EU wegfallen, langfristig soll die europäische Wirtschaftsleistung um 0,76% steigen.

Das Abkommen der EU mit Japan wurde am 12.12.2018 mit 474 Zustimmungen im Europäischen Parlament angenommen.

- Außenpolitik und internationale Beziehungen
- Außenwirtschaft
- Wirtschaft

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           | x                |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           |                |           | x                |                 |
| Markus Ferber               | CSU                           |                |           |                  | x               |
| Thomas Händel               | DIE LINKE                     |                |           | x                |                 |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                |           | x                |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                | x         |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           |                |           | x                |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 59             | 7         | 23               | 7               |
|                             | AfD                           | 0              | 1         | 0                | 0               |
|                             | CDU                           | 27             | 0         | 1                | 1               |
|                             | CSU                           | 4              | 0         | 0                | 1               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 0              | 3         | 6                | 2               |
|                             | DIE LINKE                     | 0              | 0         | 5                | 2               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 20             | 1         | 5                | 1               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/eu-japan-freihandelsabkommen-jefta)
<div style="page-break-after: always;"></div>

## EU-Richtlinien über Verwendung von Fluggastdaten

Das Parlament hat der&nbsp;neuen Richtline zur "Verwendung von Fluggastdatensätzen zu Zwecken der Verhütung, Aufdeckung, Aufklärung und strafrechtlichen Verfolgung von terroristischen Straftaten und schwerer Kriminalität" mit einer Mehrheit zugestimmt.&nbsp;

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Innere Sicherheit

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           | x                |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           |                |           | x                |                 |
| Markus Ferber               | CSU                           |                |           |                  | x               |
| Thomas Händel               | DIE LINKE                     |                |           | x                |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                |           | x                |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           |                |           | x                |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
|                             | Total                         | 46             | 1         | 35               | 13              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 23             | 0         | 0                | 6               |
|                             | CSU                           | 4              | 0         | 0                | 1               |
|                             | Die Blauen                    | 0              | 0         | 0                | 1               |
|                             | DIE GRÜNEN                    | 0              | 0         | 10               | 1               |
|                             | DIE LINKE                     | 0              | 0         | 6                | 1               |
|                             | FDP                           | 0              | 0         | 3                | 0               |
|                             | SPD                           | 12             | 1         | 11               | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/eu-richtlinien-ueber-verwendung-von-fluggastdaten)
<div style="page-break-after: always;"></div>

## EU-Sicherheitspolitik im Nahen Osten und Nordafrika

Mit der Resolution "Die sicherheitspolitischen Herausforderungen im Nahen Osten und in Nordafrika und die&nbsp;Perspektiven für politische Stabilität" hat das Europäische Parlament verschiedene Strategien beschlossen.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Außenpolitik und internationale Beziehungen
- Europapolitik und Europäische Union

| Name                    | Partei       | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| ----------------------- | ------------ | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner | ÖDP          | x              |           |                  |                 |
| Albert Deß              | CSU          | x              |           |                  |                 |
| Ismail Ertug            | SPD          |                | x         |                  |                 |
| Markus Ferber           | CSU          | x              |           |                  |                 |
| Thomas Händel           | DIE LINKE    |                |           | x                |                 |
| Monika Hohlmeier        | CSU          | x              |           |                  |                 |
| Barbara Lochbihler      | DIE GRÜNEN   | x              |           |                  |                 |
| Ulrike Müller           | FREIE WÄHLER | x              |           |                  |                 |
| Dr. Angelika Niebler    | CSU          | x              |           |                  |                 |
| Maria Noichl            | SPD          |                | x         |                  |                 |
| Manfred Weber           | CSU          | x              |           |                  |                 |
| Kerstin Westphal        | SPD          | x              |           |                  |                 |
|                         | Total        | 48             | 19        | 7                | 17              |
|                         | AfD          | 0              | 1         | 0                | 0               |
|                         | CDU          | 24             | 0         | 0                | 5               |
|                         | CSU          | 5              | 0         | 0                | 0               |
|                         | Die Blauen   | 0              | 1         | 0                | 0               |
|                         | DIE GRÜNEN   | 7              | 0         | 0                | 4               |
|                         | DIE LINKE    | 0              | 0         | 7                | 0               |
|                         | FDP          | 2              | 0         | 0                | 1               |
|                         | SPD          | 7              | 15        | 0                | 5               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/eu-sicherheitspolitik-im-nahen-osten-und-nordafrika)
<div style="page-break-after: always;"></div>

## EU-Urheberrechtsreform - Uploadfilter

Das EU-Parlament hat am 5. Juli über eine Urheberrechtsreform abgestimmt. Mit 318 (53 %) zu 278 (47 %) Stimmen wurde die Reform abgelehnt. Zu einer erneuten Abstimmung über die Änderungsanträge kam es am 12. September 2018 und diesmal war sie erfolgreich. Von den 96 deutschen EU-Abgeordneten stimmten jetzt 61 dafür, 28 dagegen, 7 waren nicht beteiligt.

- Recht

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           | x                |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           |                |           | x                |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                |           | x                |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           |                |           | x                |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 61             | 3         | 28               | 3               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 28             | 0         | 0                | 0               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 6              | 0         | 5                | 0               |
|                             | DIE LINKE                     | 0              | 0         | 6                | 1               |
|                             | FDP                           | 1              | 1         | 1                | 0               |
|                             | SPD                           | 16             | 2         | 7                | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/eu-urheberrechtsreform-uploadfilter)
<div style="page-break-after: always;"></div>

## Ein Instrument für europäische Werte zur Unterstützung zivilgesellschaftlicher Organisationen fördern

Das Europäische Parlament hat einen Entschließungsantrag zur Etablierung eines Instrumentes für europäische Werte verabschiedet. Der Antrag wurde durch die Fraktionen von den Vereinten Europäischen Linken/Nordische Grüne Linke, der Grüne/EFA, Sozialdemokraten (S&amp;D), Fraktion der Allianz der Liberalen und Demokraten für Europa (ALDE) und der Europäischen Volkspartei (EPP) unterstützt.

- Politisches Leben, Parteien

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           |                  | x               |
| Dr. Angelika Niebler        | CSU                           |                | x         |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 75             | 1         | 8                | 12              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 25             | 0         | 0                | 4               |
|                             | CSU                           | 4              | 1         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 11             | 0         | 0                | 0               |
|                             | DIE LINKE                     | 6              | 0         | 0                | 1               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 23             | 0         | 0                | 4               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/ein-instrument-fuer-europaeische-werte-zur-unterstuetzung-zivilgesellschaftlicher)
<div style="page-break-after: always;"></div>

## Entsendung von Arbeitnehmern im Rahmen der Erbringung von Dienstleistungen 

Am 29.05.2018 wurde im Europaparlament namentlich darüber abgestimmt, ob die Richtlinie aus dem Jahr 1996 zur "Entsendung von Arbeitnehmern im Rahmen der Erbringung von Dienstleistungen" nach einem Vorschlag der Kommission überarbeitet und aktualisiert werden soll. Mit 456 Zustimmungen (70%) wurde dieser Antrag angenommen. Von den 96 deutschen Abgeordneten stimmten 71 dafür, 13 dagegen, 3 enthielten sich und 9 stimmten nicht mit ab. Die Gegenstimmen lassen sich den Christdemokraten, der Alternative für Deutschland und anderen konservativen bis rechtspopulistischen Parteien zuordnen. Die Zustimmungen stammen zum größten Teil von den Sozialdemokraten, den Grünen und den Christdemokraten.

- Arbeit und Beschäftigung
- Europapolitik und Europäische Union
- Wirtschaft

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           | x                |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           |                |           | x                |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           | x                |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 72             | 3         | 12               | 9               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 22             | 3         | 3                | 1               |
|                             | CSU                           | 4              | 0         | 1                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 11             | 0         | 0                | 0               |
|                             | DIE LINKE                     | 6              | 0         | 0                | 1               |
|                             | FDP                           | 1              | 0         | 1                | 1               |
|                             | SPD                           | 23             | 0         | 0                | 4               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/entsendung-von-arbeitnehmern-im-rahmen-der-erbringung-von-dienstleistungen)
<div style="page-break-after: always;"></div>

## Entwicklungshilfe der EU für das Bildungswesen 

Der Entschließungsantrag des Europäischen Parlaments vom 15.10.2018 über die Entwicklungshilfe der EU für das Bildungswesen wurde am 13.11.2018 erfolgreich im EP angenommen. Durch diesen Antrag soll der Beitrag der Mitgliedstaaten zur Entwicklungshilfe für den Bereich der Bildung erhöht und neue Prioritäten gesetzt werden.

Mit 474 Ja-Stimmen (88%) wurde der Antrag angenommen. 61 der 96 deutschen Abgeordneten stimmten dem Entschließungsantrag zu, nur vier stimmten dagegen. Die Gegenstimmen kamen aus AfD, Die Partei und der Linken.

- Bildung und Erziehung
- Jugend
- Soziale Sicherung

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           |                |           |                  | x               |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           |                |           |                  | x               |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           |                  | x               |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           |                |           |                  | x               |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 61             | 1         | 4                | 30              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 18             | 0         | 0                | 11              |
|                             | CSU                           | 2              | 0         | 0                | 3               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 10             | 0         | 0                | 1               |
|                             | DIE LINKE                     | 3              | 0         | 1                | 3               |
|                             | FDP                           | 2              | 0         | 0                | 1               |
|                             | SPD                           | 17             | 0         | 0                | 10              |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/entwicklungshilfe-der-eu-fuer-das-bildungswesen)
<div style="page-break-after: always;"></div>

## Errichtung des Europäischen Verteidigungsfonds

In der ersten Lesung des ordentlichen Gesetzgebungsverfahrens zum Thema "Einrichtung des Europäischen Verteidigungsfonds" legte das Europaparlament verschiedene Änderungen zum Verordnungsentwurf der Kommission vor. Die Verordnung sieht eine Ausstattung von 13 Milliarden Euro für den Verteidigungsfonds vor.

Am 12.12.2018 wurde die Verordnung zur Errichtung des Europäischen Verteidigungsfonds in geänderter Fassung angenommen. 54% der MdEP stimmten dafür, von den deutschen Abgeordneten stimmten die Christdemokraten zu. Dagegen waren die Sozialdemokraten sowie die Oppositionsfraktionen. 34 deutsche Abgeordnete stimmten entgegen der Meinung ihrer Fraktion, vor allem sozialdemokratische MdEP.



	Die Verordnung wird am 1. Januar 2021 in Kraft treten.

- Europapolitik und Europäische Union
- Verteidigung
- Wirtschaft

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           |                  | x               |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           |                |           | x                |                 |
| Markus Ferber               | CSU                           |                |           |                  | x               |
| Thomas Händel               | DIE LINKE                     |                |           | x                |                 |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                |           | x                |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           |                  | x               |
| Maria Noichl                | SPD                           |                |           | x                |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           |                |           |                  | x               |
| Kerstin Westphal            | SPD                           |                |           | x                |                 |
|                             | Total                         | 37             | 6         | 44               | 9               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 28             | 0         | 0                | 1               |
|                             | CSU                           | 2              | 0         | 0                | 3               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 3              | 0         | 8                | 0               |
|                             | DIE LINKE                     | 0              | 0         | 5                | 2               |
|                             | FDP                           | 2              | 0         | 0                | 1               |
|                             | SPD                           | 0              | 6         | 20               | 1               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/errichtung-des-europaeischen-verteidigungsfonds)
<div style="page-break-after: always;"></div>

## Eu-Strategie zur Gleichstellung zwischen Männern und Frauen

Die Abgeordneten im EU-Parlament haben die Kommission dazu aufgefordert, die Rechte von Frauen und Gleichberechtigung in Europa gesetzlich zu stärken.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Gesellschaftspolitik, soziale Gruppen
- Frauen

| Name                    | Partei       | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| ----------------------- | ------------ | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner | ÖDP          |                |           | x                |                 |
| Albert Deß              | CSU          |                |           | x                |                 |
| Ismail Ertug            | SPD          | x              |           |                  |                 |
| Markus Ferber           | CSU          |                |           | x                |                 |
| Thomas Händel           | DIE LINKE    | x              |           |                  |                 |
| Monika Hohlmeier        | CSU          |                |           | x                |                 |
| Barbara Lochbihler      | DIE GRÜNEN   | x              |           |                  |                 |
| Ulrike Müller           | FREIE WÄHLER |                | x         |                  |                 |
| Dr. Angelika Niebler    | CSU          |                |           | x                |                 |
| Maria Noichl            | SPD          | x              |           |                  |                 |
| Manfred Weber           | CSU          |                |           | x                |                 |
| Kerstin Westphal        | SPD          | x              |           |                  |                 |
|                         | Total        | 46             | 3         | 38               | 4               |
|                         | AfD          | 0              | 0         | 1                | 1               |
|                         | CDU          | 0              | 1         | 27               | 1               |
|                         | CSU          | 0              | 0         | 5                | 0               |
|                         | DIE GRÜNEN   | 11             | 0         | 0                | 0               |
|                         | DIE LINKE    | 7              | 0         | 0                | 0               |
|                         | FDP          | 1              | 1         | 1                | 0               |
|                         | SPD          | 25             | 0         | 0                | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/eu-strategie-zur-gleichstellung-zwischen-maennern-und-frauen)
<div style="page-break-after: always;"></div>

## Expo 2015 in Mailand

Mit breiter Mehrheit hat das Europäische Parlament einen Entschließungsantrag zur Expo 2015 in Mailand mit dem Motto „Den Planeten ernähren, Energie für das Leben“ verabschiedet. Weitere Informationen zur Abstimmung finden Sie unten.&nbsp;

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Entwicklungspolitik
- Europapolitik und Europäische Union
- Landwirtschaft und Ernährung

| Name                    | Partei       | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| ----------------------- | ------------ | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner | ÖDP          |                |           | x                |                 |
| Albert Deß              | CSU          | x              |           |                  |                 |
| Ismail Ertug            | SPD          | x              |           |                  |                 |
| Markus Ferber           | CSU          |                |           |                  | x               |
| Thomas Händel           | DIE LINKE    |                |           |                  | x               |
| Monika Hohlmeier        | CSU          |                |           |                  | x               |
| Barbara Lochbihler      | DIE GRÜNEN   |                |           | x                |                 |
| Ulrike Müller           | FREIE WÄHLER | x              |           |                  |                 |
| Dr. Angelika Niebler    | CSU          | x              |           |                  |                 |
| Maria Noichl            | SPD          | x              |           |                  |                 |
| Manfred Weber           | CSU          | x              |           |                  |                 |
| Kerstin Westphal        | SPD          |                |           |                  | x               |
|                         | Total        | 51             | 1         | 18               | 20              |
|                         | AfD          | 0              | 0         | 2                | 0               |
|                         | CDU          | 22             | 0         | 0                | 6               |
|                         | CSU          | 3              | 0         | 0                | 2               |
|                         | DIE GRÜNEN   | 0              | 0         | 9                | 2               |
|                         | DIE LINKE    | 0              | 0         | 5                | 2               |
|                         | FDP          | 3              | 0         | 0                | 0               |
|                         | SPD          | 20             | 1         | 0                | 6               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/expo-2015-mailand)
<div style="page-break-after: always;"></div>

## Externe EU-Strategie gegen Früh- und Zwangsverheiratung

Die Entschließung des Europäischen Parlaments fordert die Mitgliedsstaaten auf, gegen Kinderehen sowie Früh- und Zwangsverheiratung vorzugehen. Mit 556 Ja-Stimmen (82%)&nbsp;wurde die Entschließung angenommen und das Parlament&nbsp;sprach sich dafür aus, diese Entschließung dem Rat und der Kommission zu übermitteln. 63 (9%) Parlamentarier*innen stimmten mit "Nein", während sich 61 (9%) enthielten. Christdemokraten, Sozialdemokraten, Liberale, Grüne und Linke stimmten für die Entschließung. Die nationalistischen und rechtsextremistischen Fraktionen konnten keine gemeinsame Position finden, stimmten aber oft mit "Nein".

Von den 96 deutschen EU-Abgeordneten stimmten 77 für die Entschließung, 7 dagegen, 2 waren nicht beteiligt und 10 enthielten sich ihrer Stimme.

- Außenpolitik und internationale Beziehungen
- Frauen
- Jugend
- Menschenrechte

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           |                |           | x                |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                | x         |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 77             | 10        | 7                | 2               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 19             | 8         | 2                | 0               |
|                             | CSU                           | 2              | 1         | 2                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 11             | 0         | 0                | 0               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 27             | 0         | 0                | 0               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/externe-eu-strategie-gegen-frueh-und-zwangsverheiratung)
<div style="page-break-after: always;"></div>

## Festsetzung des Zeitraums für die neunte  Wahl des EU Parlaments

Die legislative Entschließung des EU-Parlaments legt den Zeitraum der nächsten Wahl der Abgeordneten des Europäischen Parlaments fest. Diese wird im Zeitraum vom 23. bis 26. Mai 2019 abgehalten. Mit 492 Ja-Stimmen (93%)&nbsp;wurde die Entschließung angenommen. 14 (3%) Parlamentarier*innen stimmten mit "Nein", während sich 24 (5%) enthielten.

- Europapolitik und Europäische Union
- Parlamentsangelegenheiten

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                | x         |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           |                |           |                  | x               |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           |                |           |                  | x               |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           |                  | x               |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           |                |           |                  | x               |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 53             | 1         | 6                | 36              |
|                             | AfD                           | 1              | 0         | 0                | 0               |
|                             | CDU                           | 20             | 0         | 0                | 9               |
|                             | CSU                           | 2              | 0         | 0                | 3               |
|                             | Die Blauen                    | 1              | 0         | 0                | 0               |
|                             | DIE GRÜNEN                    | 9              | 0         | 0                | 2               |
|                             | DIE LINKE                     | 2              | 0         | 0                | 5               |
|                             | FDP                           | 1              | 0         | 0                | 2               |
|                             | SPD                           | 15             | 0         | 0                | 12              |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/festsetzung-des-zeitraums-fuer-die-neunte-wahl-des-eu-parlaments)
<div style="page-break-after: always;"></div>

## Fortschritte beim globalen Pakt für Flüchtlinge

Die Entschließung des Europäischen Parlaments begrüßt den globalen Pakt für Geflüchtete und fordert die Mitgliedsstaaten auf, sich auf eine einheitliche EU-Position zu verständigen, um geordnete und reguläre Migration voranzubringen. Mit 516 Ja-Stimmen (74%)&nbsp;wurde die Entschließung angenommen. 137 (20%) Parlamentarier*innen stimmten mit "Nein", während sich 43 (6%) enthielten. Sozialdemokraten, Christdemokraten, Liberale, Linke und Grüne stimmten für die Entschließung. Die nationalistischen und rechtsextremen Fraktionen stimmten zumeist dagegen, zeigten aber deutliche Uneinigkeit. Von den 96 deutschen EU-Abgeordneten stimmten 78 für die Entschließung, 10 dagegen und 8 waren nicht beteiligt.

Das Parlament&nbsp;beauftragt seinen Präsidenten, diese Entschließung dem Rat und der Kommission zu übermitteln.

- Ausländerpolitik, Zuwanderung
- Entwicklungspolitik
- Humanitäre Hilfe

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 78             | 0         | 10               | 8               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 25             | 0         | 2                | 2               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 11             | 0         | 0                | 0               |
|                             | DIE LINKE                     | 6              | 0         | 0                | 1               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 24             | 0         | 0                | 3               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/fortschritte-beim-globalen-pakt-fuer-fluechtlinge)
<div style="page-break-after: always;"></div>

## Freiheit und Pluralismus der Medien in der EU 

Das Europäische Parlament hat einen Entschließungsantrag für Freiheit und Pluralismus der Medien in der EU angenommen. In der Resolution werden Mitgliedsstaaten sowie die Organe der Europäischen Union dazu aufgefordert, den Zustand der Meinungs- und Informationsfreiheit zu verbessern.

- Europapolitik und Europäische Union
- Medien, Kommunikation und Informationstechnik
- Medien
- Recht
- Menschenrechte

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                | x         |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 76             | 3         | 5                | 12              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 24             | 2         | 0                | 3               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 10             | 0         | 0                | 1               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 22             | 0         | 0                | 5               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/freiheit-und-pluralismus-der-medien-der-eu)
<div style="page-break-after: always;"></div>

## Förderung junger Unternehmer

Das Europäische Parlament hat sich für einen Text mit dem Ziel der&nbsp;"Förderung des Unternehmergeists junger Menschen durch Bildung und Ausbildung" ausgesprochen.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Bildung und Erziehung
- Europapolitik und Europäische Union
- Wirtschaft

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                | x         |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                | x         |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 69             | 11        | 10               | 5               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 26             | 0         | 0                | 3               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 2              | 9         | 0                | 0               |
|                             | DIE LINKE                     | 5              | 0         | 2                | 0               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 26             | 0         | 0                | 1               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/foerderung-junger-unternehmer)
<div style="page-break-after: always;"></div>

## Förderung von Energie aus erneuerbaren Quellen

Das Europäische Parlament stimmte dem Gesetzesentwurf über "Förderung der Nutzung von Energie aus erneuerbaren Quellen" mit 72 Prozent Ja-Stimmen zu. Der Entwurf wurde vom&nbsp;Ausschuss für Industrie, Forschung und Energie erarbeitet und eingebracht. Dieser spricht sich für einen Gesamtrahmen für Energie aus erneuerbaren Quellen aus, der es ermöglicht, die Zielvorgaben für 2030 und die langfristigen Ziele für 2050 zu erreichen. Grüne, Sozialisten, Liberale und Christdemokraten stimmten dem Entwurf zu (492 Ja-Stimmen), dem standen 88 Nein-Stimmen (13%) und 107&nbsp;Enthaltungen (16%) gegenüber.

Bitte beachten Sie, dass wir nur für die deutschen EU-Abgeordneten&nbsp;das individuelle Abstimmungsverhalten darstellen.

- Energie
- Umwelt
- Naturschutz
- Wissenschaft, Forschung und Technologie

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           |                |           |                  | x               |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 81             | 6         | 6                | 3               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 26             | 0         | 3                | 0               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 11             | 0         | 0                | 0               |
|                             | DIE LINKE                     | 0              | 6         | 0                | 1               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 25             | 0         | 0                | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/foerderung-von-energie-aus-erneuerbaren-quellen)
<div style="page-break-after: always;"></div>

## Gemeinsame Strategie zur Prävention von Radikalisierung

Die&nbsp;Abgeordneten im EU-Parlament haben in einer Resolution ein europaweit koordiniertes Vorgehen gegen Terrorverdächtige gefordert.&nbsp;

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Innere Sicherheit

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           | x                |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                |           |                  | x               |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           |                |           | x                |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 67             | 2         | 18               | 9               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 25             | 0         | 0                | 4               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 9              | 0         | 0                | 2               |
|                             | DIE LINKE                     | 0              | 1         | 5                | 1               |
|                             | FDP                           | 1              | 0         | 2                | 0               |
|                             | SPD                           | 19             | 0         | 6                | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/gemeinsame-strategie-zur-praevention-von-radikalisierung)
<div style="page-break-after: always;"></div>

## Gender Mainstreaming

Der Antrag zur&nbsp;Gender-Mainstreaming-Strategie, über den am&nbsp;15.01.2019 abgestimmt wurde, fordert Parlament, Kommission und Rat auf, dass bei der Erstellung des mehrjährigen&nbsp;Finanzrahmens (MFR) der EU immer auch Gleichstellungsaspekte berücksichtigt werden sollen. Außerdem wird die Kommission aufgefordert, eine Gleichstellungsstrategie vorzulegen, die eindeutige Ziele beinhaltet und in allen Amtssprachen der EU übersetzt werden solle, um für einen möglichst hohen Bekanntheitsgrad bei der Zivilgesellschaft&nbsp;wie auch bei wirtschaftlichen Akteuren sorgen solle.&nbsp;

Der&nbsp;Antrag wurde von den deutschen EU-Abgeordneten mehrheitlich angenommen. Gegenstimmen kamen vor allem seitens der Christdemokraten und der Konservativen. Insgesamt stimmten alle EU-Abgeordneten mehrheitlich dafür. Somit wurde der Antrag angenommen.

- Gesellschaftspolitik, soziale Gruppen
- Frauen

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           | x                |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     | x              |           |                  |                 |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 71             | 6         | 15               | 3               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 19             | 6         | 4                | 0               |
|                             | CSU                           | 4              | 0         | 1                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 10             | 0         | 0                | 1               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 26             | 0         | 0                | 0               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/gender-mainstreaming)
<div style="page-break-after: always;"></div>

## Gleichstellung der Geschlechter in Handelsabkommen der EU

Die Entschließung des Europäischen Parlaments fordert die Mitgliedsstaaten auf, der Gleichstellung der Geschlechter in ihrer Handelspolitik Rechnung zu tragen. Mit 512 Ja-Stimmen (75%)&nbsp;wurde die Entschließung angenommen. 107 (16%) Parlamentarier*innen stimmten mit "Nein", während sich 68 (10%) enthielten. Christdemokraten, Sozialdemokraten, Liberale, Grüne und Linke stimmten für die Entschließung. Die Mitglieder der anderen Fraktionen zumeist dagegen, bei ihnen war aber kein einheitliches Bild zu erkennen.

Von den 96 deutschen EU-Abgeordneten stimmten 76 für die Entschließung, 10 dagegen, 6 waren nicht beteiligt und 4 enthielten sich. Das Parlament&nbsp;beauftragt seinen Präsidenten, diese Entschließung dem Rat und der Kommission zu übermitteln.

- Frauen
- Menschenrechte
- Wirtschaft

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                | x         |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                | x         |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           |                |           |                  | x               |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 76             | 5         | 10               | 5               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 25             | 2         | 1                | 1               |
|                             | CSU                           | 2              | 1         | 1                | 1               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 10             | 0         | 0                | 1               |
|                             | DIE LINKE                     | 6              | 0         | 0                | 1               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 27             | 0         | 0                | 0               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/gleichstellung-der-geschlechter-handelsabkommen-der-eu)
<div style="page-break-after: always;"></div>

## Gleichstellung der Geschlechter in der Steuerpolitik

Der&nbsp;Entschließungsantrag, über den das Europäische Parlament am&nbsp;15. Januar 2019 abstimmte, fordert die Kommission auf, im Rahmen all ihrer steuerpolitischen Maßnahmen die Gleichstellung der Geschlechter zu fördern, um geschlechtsbezogene Verzerrungseffekte bei den Steuern zukünftig vermeiden zu können.

Der Antrag wurde mit 46 Prozent aller Stimmen angenommen. Unter den Ergebnissen bei den deutschen Abgeordneten übertrafen die Ablehnungen die Zustimmungen nur mit einer Stimme. Konservative und rechtspopulistische Fraktionen sprachen sich gegen den Antrag aus, die Fraktionen der Sozialdemokraten und der Grünen stimmten dem Antrag zu.

- Gesellschaftspolitik, soziale Gruppen
- Öffentliche Finanzen, Steuern und Abgaben

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                | x         |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           | x                |                 |
| Thomas Händel               | DIE LINKE                     | x              |           |                  |                 |
| Nadja Hirsch                | FDP                           |                |           | x                |                 |
| Monika Hohlmeier            | CSU                           |                |           | x                |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                | x         |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           | x                |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           |                | x         |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 42             | 4         | 43               | 6               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 0              | 1         | 27               | 1               |
|                             | CSU                           | 0              | 1         | 4                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 10             | 0         | 0                | 1               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 0              | 0         | 3                | 0               |
|                             | SPD                           | 24             | 0         | 0                | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/gleichstellung-der-geschlechter-in-der-steuerpolitik)
<div style="page-break-after: always;"></div>

## Haushaltsplan 2019

Zuvor hatte der Europäische Rat Änderungen zum zweiten Entwurf des Gesamthaushaltsplans der EU für das Jahr 2019 vorgelegt. Nun brachte das Europaparlament einen Entschließungsantrag zu diesem Entwurf vor, über den am 12.12.2018 abgestimmt wurde. Inhalte des Haushaltsplans sind unter anderem die Bekämpfung von Migration, Unterstützung von Forschern sowie von Jugendlichen und das Aufhalten des Klimawandels.

Mit 67% Zustimmungen der Abgeordneten wurde der Entschließungsantrag angenommen. Deutsche MdEP der Christdemokraten und der Sozialdemokraten stimmten geschlossen zu, in den Reihen der deutschen Oppositionsparteien wurde der Antrag abgelehnt oder nicht bewertet. Mit dem Antrag fordert das Europaparlament seinen Präsidenten Antonio Tajani auf, den Gesamthaushaltsplan für das Jahr 2019 zu verabschieden.

- Europapolitik und Europäische Union
- Haushalt
- Wirtschaft

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                | x         |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           |                  | x               |
| Thomas Händel               | DIE LINKE                     |                |           | x                |                 |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                | x         |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 63             | 12        | 16               | 5               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 28             | 0         | 0                | 1               |
|                             | CSU                           | 4              | 0         | 0                | 1               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 1              | 10        | 0                | 0               |
|                             | DIE LINKE                     | 0              | 0         | 5                | 2               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 26             | 0         | 0                | 1               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/haushaltsplan-2019)
<div style="page-break-after: always;"></div>

## Hindernisse für soziale Unternehmen beseitigen

Mit breiter Mehrheit hat sich das EU-Parlament dafür ausgesprochen, Hindernisse für soziale Unternehmen zu beseitigen.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Öffentliche Finanzen, Steuern und Abgaben
- Wirtschaft

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           |                  | x               |
| Thomas Händel               | DIE LINKE                     |                |           | x                |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 73             | 1         | 8                | 13              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 27             | 0         | 0                | 2               |
|                             | CSU                           | 4              | 0         | 0                | 1               |
|                             | Die Blauen                    | 1              | 0         | 0                | 0               |
|                             | DIE GRÜNEN                    | 9              | 0         | 0                | 2               |
|                             | DIE LINKE                     | 0              | 0         | 7                | 0               |
|                             | FDP                           | 1              | 1         | 0                | 1               |
|                             | SPD                           | 22             | 0         | 0                | 5               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/hindernisse-fuer-soziale-unternehmen-beseitigen)
<div style="page-break-after: always;"></div>

## Kennzeichnung des Ursprungslands von Fleisch

Das Europäische Parlament hat mehrheitlich eine Kennzeichnungspflicht für das Ursprungsland von Fleisch in verarbeiteten Lebensmitteln beschlossen.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Landwirtschaft und Ernährung

| Name                    | Partei       | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| ----------------------- | ------------ | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner | ÖDP          | x              |           |                  |                 |
| Albert Deß              | CSU          |                |           | x                |                 |
| Ismail Ertug            | SPD          | x              |           |                  |                 |
| Markus Ferber           | CSU          |                |           | x                |                 |
| Thomas Händel           | DIE LINKE    |                |           |                  | x               |
| Monika Hohlmeier        | CSU          |                |           | x                |                 |
| Barbara Lochbihler      | DIE GRÜNEN   | x              |           |                  |                 |
| Ulrike Müller           | FREIE WÄHLER | x              |           |                  |                 |
| Dr. Angelika Niebler    | CSU          |                |           | x                |                 |
| Maria Noichl            | SPD          | x              |           |                  |                 |
| Manfred Weber           | CSU          |                |           | x                |                 |
| Kerstin Westphal        | SPD          | x              |           |                  |                 |
|                         | Total        | 52             | 0         | 34               | 5               |
|                         | AfD          | 2              | 0         | 0                | 0               |
|                         | CDU          | 2              | 0         | 27               | 0               |
|                         | CSU          | 0              | 0         | 5                | 0               |
|                         | DIE GRÜNEN   | 10             | 0         | 0                | 1               |
|                         | DIE LINKE    | 6              | 0         | 0                | 1               |
|                         | FDP          | 0              | 0         | 2                | 1               |
|                         | SPD          | 25             | 0         | 0                | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/kennzeichnung-des-ursprungslands-von-fleisch)
<div style="page-break-after: always;"></div>

## Klimadiplomatie

Die Entschließung des Europäischen Parlaments fordert die EU und ihre Mitgliedstaaten auf, Klimaschutzmaßnahmen verstärkt auf die Tagesordnung von Gipfeltreffen zu setzten. Mit 488 Ja-Stimmen (73%)&nbsp;wurde die Entschließung angenommen. 113 (17%) Parlamentarier*innen stimmten mit "Nein", während sich 72 (11%) enthielten. Das Parlament&nbsp;beauftragt seinen Präsidenten, diese Entschließung dem Rat und der Kommission zu übermitteln.

Von den 96 deutschen EU-Abgeordneten stimmten 75 für die Entschließung, 12 dagegen, 6 waren nicht beteiligt und 3 enthielten sich ihrer Stimme.

- Außenpolitik und internationale Beziehungen
- Energie
- Umwelt

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 75             | 3         | 12               | 5               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 24             | 2         | 2                | 1               |
|                             | CSU                           | 4              | 0         | 1                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 9              | 0         | 0                | 1               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 26             | 0         | 0                | 1               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/klimadiplomatie)
<div style="page-break-after: always;"></div>

## Kreditvergabe an die Ukraine

Die EU-Parlamentarier haben in erster Lesung mit großer Mehrheit einen Vorschlag der Kommission angenommen, wonach der Ukraine Kredite im Umfang von 1,8 Milliarden Euro gewährt werden sollen.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Öffentliche Finanzen, Steuern und Abgaben
- Finanzen

| Name                    | Partei       | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| ----------------------- | ------------ | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner | ÖDP          | x              |           |                  |                 |
| Albert Deß              | CSU          | x              |           |                  |                 |
| Ismail Ertug            | SPD          | x              |           |                  |                 |
| Markus Ferber           | CSU          | x              |           |                  |                 |
| Thomas Händel           | DIE LINKE    |                |           |                  | x               |
| Monika Hohlmeier        | CSU          | x              |           |                  |                 |
| Barbara Lochbihler      | DIE GRÜNEN   | x              |           |                  |                 |
| Ulrike Müller           | FREIE WÄHLER | x              |           |                  |                 |
| Dr. Angelika Niebler    | CSU          |                |           |                  | x               |
| Maria Noichl            | SPD          | x              |           |                  |                 |
| Manfred Weber           | CSU          |                |           |                  | x               |
| Kerstin Westphal        | SPD          | x              |           |                  |                 |
|                         | Total        | 63             | 0         | 6                | 22              |
|                         | AfD          | 0              | 0         | 1                | 1               |
|                         | CDU          | 25             | 0         | 0                | 4               |
|                         | CSU          | 3              | 0         | 0                | 2               |
|                         | DIE GRÜNEN   | 10             | 0         | 0                | 1               |
|                         | DIE LINKE    | 0              | 0         | 4                | 3               |
|                         | FDP          | 2              | 0         | 0                | 1               |
|                         | SPD          | 19             | 0         | 0                | 8               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/kreditvergabe-die-ukraine)
<div style="page-break-after: always;"></div>

## Kritische Neubewertung der Beziehungen zu Russland

Das Europäische Parlament hat sich mehrheitlich für einen Antrag mit dem Titel "Stand der Beziehungen EU-Russland" ausgesprochen.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Außenpolitik und internationale Beziehungen
- Europapolitik und Europäische Union

| Name                    | Partei       | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| ----------------------- | ------------ | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner | ÖDP          | x              |           |                  |                 |
| Albert Deß              | CSU          | x              |           |                  |                 |
| Ismail Ertug            | SPD          |                | x         |                  |                 |
| Markus Ferber           | CSU          | x              |           |                  |                 |
| Thomas Händel           | DIE LINKE    |                |           | x                |                 |
| Monika Hohlmeier        | CSU          | x              |           |                  |                 |
| Barbara Lochbihler      | DIE GRÜNEN   | x              |           |                  |                 |
| Ulrike Müller           | FREIE WÄHLER | x              |           |                  |                 |
| Dr. Angelika Niebler    | CSU          | x              |           |                  |                 |
| Maria Noichl            | SPD          | x              |           |                  |                 |
| Manfred Weber           | CSU          | x              |           |                  |                 |
| Kerstin Westphal        | SPD          | x              |           |                  |                 |
|                         | Total        | 70             | 6         | 11               | 4               |
|                         | AfD          | 0              | 0         | 2                | 0               |
|                         | CDU          | 28             | 0         | 1                | 0               |
|                         | CSU          | 5              | 0         | 0                | 0               |
|                         | DIE GRÜNEN   | 8              | 1         | 0                | 2               |
|                         | DIE LINKE    | 0              | 0         | 7                | 0               |
|                         | FDP          | 2              | 0         | 0                | 1               |
|                         | SPD          | 23             | 3         | 0                | 1               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/kritische-neubewertung-der-beziehungen-zu-russland)
<div style="page-break-after: always;"></div>

## Lage der Grundrechte in der EU 2016

Die Entschließung des Europäischen Parlaments fordert die Mitgliedsstaaten auf, für Verbesserung der Menschenrechtssituation in ihrem Einflussbereich zu sorgen. Mit 429 Ja-Stimmen (68%)&nbsp;wurde die Entschließung angenommen und das Parlament&nbsp;sprach sich dafür aus, diese Entschließung dem Rat und der Kommission zu übermitteln. 135 (22%) Parlamentarier*innen stimmten mit "Nein", während sich 63 (10%) enthielten. Christdemokraten, Sozialdemokraten, Liberale und GRÜNE stimmten für die Entschließung. Die Linke Fraktion&nbsp;GUE/NGL fand keine gemeinsame Position.Zumeist mit "Nein" stimmten die nationalistischen und rechtsextremistischen Fraktionen.

Von den 96 deutschen EU-Abgeordneten stimmten 67 für die Entschließung, 7 dagegen, 14 waren nicht beteiligt und 8 enthielten sich ihrer Stimme.

- Europapolitik und Europäische Union
- Recht
- Menschenrechte

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           |                |           |                  | x               |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           |                |           |                  | x               |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                |           |                  | x               |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 67             | 8         | 7                | 14              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 23             | 3         | 0                | 3               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 0                | 1               |
|                             | DIE GRÜNEN                    | 9              | 0         | 0                | 2               |
|                             | DIE LINKE                     | 2              | 3         | 1                | 1               |
|                             | FDP                           | 2              | 0         | 0                | 1               |
|                             | SPD                           | 22             | 0         | 1                | 4               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/lage-der-grundrechte-der-eu-2016)
<div style="page-break-after: always;"></div>

## Leitlinien für die Beziehungen mit dem Vereinigten Königreich

Der gemeinsame Entschließungsantrag von den Fraktionen der EPP, S&amp;D, ALDE, GUE/NGL und Grüne EFA wurde vom Europäischen Parlament als Resolution angenommen. Sie entwirft Grundsätze für die Beziehungen der EU zum Vereinigten Königreich nach dessen Austritt aus der Europäischen Union.

- Außenpolitik und internationale Beziehungen
- Europapolitik und Europäische Union

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 81             | 2         | 7                | 5               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 27             | 0         | 0                | 2               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 10             | 0         | 0                | 1               |
|                             | DIE LINKE                     | 5              | 1         | 0                | 1               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 26             | 0         | 0                | 0               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/leitlinien-fuer-die-beziehungen-mit-dem-vereinigten-koenigreich)
<div style="page-break-after: always;"></div>

## Manipulation des Kilometerzählers in Kraftfahrzeugen

Die Entschließung des Europäischen Parlaments fordert die Mitgliedsstaaten auf, Rechtsvorschriften im Bereich der Manipulation von Kilometerzählern zu schaffen oder anzupassen, sodass diese als Straftat gilt. Mit 577 Ja-Stimmen (92%)&nbsp;wurde die Entschließung angenommen. 32 (5%) Parlamentarier*innen stimmten mit "Nein", während sich 19 (3%) enthielten. Von den 96 deutschen EU-Abgeordneten stimmten 79 für die Entschließung, 2 dagegen und 14 waren nicht beteiligt. Es gab eine Enthaltung.

Das Parlament&nbsp;beauftragt seinen Präsidenten, diese Entschließung dem Rat und der Kommission zu übermitteln.

- Verbraucherschutz
- Umwelt
- Verkehr

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           |                  | x               |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 79             | 1         | 2                | 14              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 26             | 0         | 0                | 3               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 1         | 0                | 0               |
|                             | DIE GRÜNEN                    | 9              | 0         | 0                | 2               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 1              | 0         | 0                | 2               |
|                             | SPD                           | 24             | 0         | 0                | 3               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/manipulation-des-kilometerzaehlers-kraftfahrzeugen)
<div style="page-break-after: always;"></div>

## Maßnahmen zur Flüchtlingspolitik

Angesichts der hohen Anzahl an Flüchtlingen und Migranten, die derzeit Opfer von Konflikten, Menschenrechtsverletzungen, Staatsversagens und Unterdrückung sind, hat das Europäische Parlament einen Antrag zur Flüchtlingspolitik verabschiedet. Weitere Informationen finden Sie unten.&nbsp;

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Außenpolitik und internationale Beziehungen
- Ausländerpolitik, Zuwanderung
- Europapolitik und Europäische Union

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 73             | 2         | 8                | 12              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 23             | 2         | 0                | 4               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 10             | 0         | 0                | 1               |
|                             | DIE LINKE                     | 7              | 0         | 0                | 0               |
|                             | FDP                           | 2              | 0         | 0                | 1               |
|                             | SPD                           | 23             | 0         | 0                | 4               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/massnahmen-zur-fluechtlingspolitik)
<div style="page-break-after: always;"></div>

## Maßnahmen zur Terrorismusbekämpfung

Das EU-Parlament hat sich für neue Maßnahmen zur Terrorismusbekämpfung ausgesprochen.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Innere Sicherheit

| Name                    | Partei       | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| ----------------------- | ------------ | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner | ÖDP          |                |           | x                |                 |
| Albert Deß              | CSU          | x              |           |                  |                 |
| Ismail Ertug            | SPD          | x              |           |                  |                 |
| Markus Ferber           | CSU          | x              |           |                  |                 |
| Thomas Händel           | DIE LINKE    |                |           |                  | x               |
| Monika Hohlmeier        | CSU          | x              |           |                  |                 |
| Barbara Lochbihler      | DIE GRÜNEN   |                |           | x                |                 |
| Ulrike Müller           | FREIE WÄHLER | x              |           |                  |                 |
| Dr. Angelika Niebler    | CSU          | x              |           |                  |                 |
| Maria Noichl            | SPD          |                |           | x                |                 |
| Manfred Weber           | CSU          | x              |           |                  |                 |
| Kerstin Westphal        | SPD          | x              |           |                  |                 |
|                         | Total        | 60             | 4         | 22               | 5               |
|                         | AfD          | 2              | 0         | 0                | 0               |
|                         | CDU          | 29             | 0         | 0                | 0               |
|                         | CSU          | 5              | 0         | 0                | 0               |
|                         | DIE GRÜNEN   | 0              | 0         | 10               | 1               |
|                         | DIE LINKE    | 0              | 0         | 6                | 1               |
|                         | FDP          | 2              | 0         | 0                | 1               |
|                         | SPD          | 18             | 3         | 4                | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/massnahmen-zur-terrorismusbekaempfung)
<div style="page-break-after: always;"></div>

## Menschenrechte und Migration in Drittländern

Das EU-Parlament hat einer umfassenden Resolution zum Thema Flucht und Migration&nbsp;zugestimmt.&nbsp;

Die knappe Mehrheit von 49% (339 Stimmen) gegen 48% Ablehnung (333 Stimmen) wurde geformt durch die Fraktionen der Europäischen Linken, Grünen, Sozialdemokraten und Liberalen.&nbsp;



	&nbsp;



	Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Ausländerpolitik, Zuwanderung
- Europapolitik und Europäische Union

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           | x                |                 |
| Thomas Händel               | DIE LINKE                     | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           |                |           | x                |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           | x                |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           |                |           | x                |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 46             | 1         | 44               | 5               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 0              | 0         | 29               | 0               |
|                             | CSU                           | 0              | 0         | 5                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 10             | 1         | 0                | 0               |
|                             | DIE LINKE                     | 6              | 0         | 0                | 1               |
|                             | FDP                           | 2              | 0         | 1                | 0               |
|                             | SPD                           | 25             | 0         | 0                | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/menschenrechte-und-migration-drittlaendern)
<div style="page-break-after: always;"></div>

## Mindestnormen für den Schutz von Minderheiten in der EU

Am 13.11.2018 wurde im Europäischen Parlament über einen Entschließungsantrag des Ausschusses für bürgerliche Freiheiten, Justiz und Inneres zu "Mindestnormen für den Schutz von Minderheiten in der EU" abgestimmt. Der Antrag forderte umfassende Maßnahmen für verbesserte Rechte und größeren Schutz der Minderheiten in den EU-Mitgliedstaaten und verlangt von der Kommission, ein Gremium für die Anerkennung und der Schutz von Minderheiten auf Unionsebene einzurichten.

Der Entschließungsantrag wurde mit einer großen Mehrheit von 489 der 674 (73%) teilnehmenden MdEP angenommen. Die deutschen Abgeordneten stimmten ebenfalls mehrheitlich dafür, die sechs Gegenstimmen unter den 96 abgegebenen Stimmen kamen aus der AfD, der NPD und von parteilosen MdEP.

- Ausländerpolitik, Zuwanderung
- Gesellschaftspolitik, soziale Gruppen
- Recht

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 84             | 2         | 6                | 4               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 28             | 0         | 0                | 1               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 1         | 0                | 0               |
|                             | DIE GRÜNEN                    | 11             | 0         | 0                | 0               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 2              | 0         | 0                | 1               |
|                             | SPD                           | 27             | 0         | 0                | 0               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/mindestnormen-fuer-den-schutz-von-minderheiten-der-eu)
<div style="page-break-after: always;"></div>

## Nachhaltige Verwendung von Pestiziden

Die Mitgliedstaaten und die Europäische Kommission werden in dem Entschließungsantrag des Europaparlaments aufgefordert, die schon bestehende Richtlinie aus dem Jahr 2009 zur Nachhaltigen Verwendung von Pestiziden umgehend vollständig umzusetzen und erforderliche Maßnahmen zu ergreifen, um risikoarme Pestizide zu fördern.

Unter namentlicher Abstimmung hat das Europaparlament den Antrag mit 84% Zustimmungen angenommen. Die deutschen EU-Abgeordneten stimmten dem Antrag fast ausschließlich zu, nur drei MdEP stimmten dagegen. Die Gegenstimmen kamen aus konservativen bis rechtspopulistischen Fraktionen.

- Europapolitik und Europäische Union
- Landwirtschaft und Ernährung
- Umwelt

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           |                  | x               |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           |                  | x               |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 76             | 4         | 3                | 13              |
|                             | AfD                           | 0              | 1         | 0                | 0               |
|                             | CDU                           | 24             | 2         | 0                | 3               |
|                             | CSU                           | 3              | 0         | 1                | 1               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 11             | 0         | 0                | 0               |
|                             | DIE LINKE                     | 6              | 0         | 0                | 1               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 22             | 0         | 0                | 5               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/nachhaltige-verwendung-von-pestiziden)
<div style="page-break-after: always;"></div>

## Nutzung der Daten von Facebook-Usern durch Cambridge Analytica

Am 16. Oktober 2018 wurde im Europäischen Parlament über einen Entschließungsantrag "zu der Nutzung der Daten von Facebook-Nutzern durch Cambridge Analytica und den Auswirkungen auf den Datenschutz" abgestimmt. Der Entschließungsantrag forderte Facebook auf, sich einer ausgiebigen Prüfung der Plattform durch ENISA und den Europäischen Datenschutzausschuss zu stellen, um die Ergebnisse Kommission und Parlament vorzulegen. Die Kommission wird außerdem aufgefordert, den Schutz der personenbezogenen Daten durch Rechtsvorschriften zu verstärken. Dadurch soll für mehr Transparenz und Rechenschaftspflicht in der Datenverarbeitung gesorgt werden.

Nur 354 der 751 Abgeordneten nahmen an der Abstimmung teil, davon stimmten auch nur 30 MdEP (8%) mit JA, sodass der Antrag abgelehnt wurde.



	Von den 96 deutschen Abgeordneten nahmen 49 Personen an der Abstimmung teil. Lediglich drei Zustimmungen kamen aus&nbsp;CDU,&nbsp;AfD und&nbsp;NPD, die Gegenstimmen aus verschiedenen Fraktionen.

- Europapolitik und Europäische Union
- Medien, Kommunikation und Informationstechnik
- Recht

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           | x                |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           |                |           | x                |                 |
| Markus Ferber               | CSU                           |                |           |                  | x               |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           |                |           |                  | x               |
| Monika Hohlmeier            | CSU                           |                |           | x                |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                |           | x                |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           |                  | x               |
| Dr. Angelika Niebler        | CSU                           |                |           |                  | x               |
| Maria Noichl                | SPD                           |                |           | x                |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           |                  | x               |
| Manfred Weber               | CSU                           |                |           | x                |                 |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 3              | 1         | 45               | 47              |
|                             | AfD                           | 1              | 0         | 0                | 0               |
|                             | CDU                           | 1              | 0         | 15               | 13              |
|                             | CSU                           | 0              | 0         | 3                | 2               |
|                             | Die Blauen                    | 0              | 1         | 0                | 0               |
|                             | DIE GRÜNEN                    | 0              | 0         | 5                | 6               |
|                             | DIE LINKE                     | 0              | 0         | 3                | 4               |
|                             | FDP                           | 0              | 0         | 1                | 2               |
|                             | SPD                           | 0              | 0         | 13               | 14              |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/nutzung-der-daten-von-facebook-usern)
<div style="page-break-after: always;"></div>

## Parlamentarische Empfehlung bzgl. TTIP Handelabkommen

Das Europäische Parlament hat im Zusammenhang mit dem Freihandelsabkommen TTIP die garantierte Sicherung von europäischen Grundrechtsnormen gefordert.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Außenwirtschaft
- Europapolitik und Europäische Union

| Name                    | Partei       | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| ----------------------- | ------------ | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner | ÖDP          |                |           | x                |                 |
| Albert Deß              | CSU          | x              |           |                  |                 |
| Ismail Ertug            | SPD          |                |           | x                |                 |
| Markus Ferber           | CSU          | x              |           |                  |                 |
| Thomas Händel           | DIE LINKE    |                |           | x                |                 |
| Monika Hohlmeier        | CSU          | x              |           |                  |                 |
| Barbara Lochbihler      | DIE GRÜNEN   |                |           | x                |                 |
| Ulrike Müller           | FREIE WÄHLER | x              |           |                  |                 |
| Dr. Angelika Niebler    | CSU          | x              |           |                  |                 |
| Maria Noichl            | SPD          |                |           | x                |                 |
| Manfred Weber           | CSU          | x              |           |                  |                 |
| Kerstin Westphal        | SPD          | x              |           |                  |                 |
|                         | Total        | 60             | 1         | 26               | 4               |
|                         | AfD          | 0              | 0         | 1                | 0               |
|                         | CDU          | 27             | 0         | 0                | 2               |
|                         | CSU          | 5              | 0         | 0                | 0               |
|                         | Die Blauen   | 0              | 0         | 1                | 0               |
|                         | DIE GRÜNEN   | 0              | 0         | 10               | 1               |
|                         | DIE LINKE    | 0              | 0         | 7                | 0               |
|                         | FDP          | 3              | 0         | 0                | 0               |
|                         | SPD          | 23             | 0         | 3                | 1               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/parlamentarische-empfehlung-bzgl-ttip-handelabkommen)
<div style="page-break-after: always;"></div>

## Persistente organische Schadstoffe

Die EU-Abgeordneten stimmten am 15.11.2018 über einen legislativen Entschließungsantrag zum Thema persistente organische Schadstoffe ab.&nbsp;Durch den&nbsp;Neufassungsvorschlag&nbsp;soll die Verordnung an den Vertrag von Lissabon und die Änderungen des Stockholmer Übereinkommens&nbsp;angepasst werden.

92% der anwesenden MdEP stimmten für den Antrag. Somit wurde dieser angenommen.&nbsp;

- Umwelt
- Naturschutz

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           |                |           |                  | x               |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 83             | 0         | 2                | 11              |
|                             | AfD                           | 0              | 0         | 0                | 1               |
|                             | CDU                           | 26             | 0         | 0                | 3               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 10             | 0         | 0                | 1               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 1              | 0         | 0                | 2               |
|                             | SPD                           | 25             | 0         | 0                | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/persistente-organische-schadstoffe)
<div style="page-break-after: always;"></div>

## Rechtsstaatsverfahren gegen Ungarn 

Mit der Abstimmung am 12. September 2018 wurde das Strafverfahren gegen Ungarn in Gang gesetzt. Mit 448 Ja-Stimmen wurde die benötigte Zwei-Drittel-Mehrheit erreicht. Von den 96 deutschen Abgeordneten waren 75 dafür, 14 dagegen, 4 enthielten sich und 3 stimmten nicht mit ab. Die Gegenstimmen kamen hauptsächlich aus den konservativeren Parteien, wie der CDU/CSU, AfD, NPD und der Familien-Partei.

- Außenpolitik und internationale Beziehungen
- Europapolitik und Europäische Union
- Recht

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           | x                |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           |                |           | x                |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           | x                |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 75             | 4         | 14               | 2               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 23             | 4         | 1                | 0               |
|                             | CSU                           | 1              | 0         | 4                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 11             | 0         | 0                | 0               |
|                             | DIE LINKE                     | 6              | 0         | 0                | 1               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 26             | 0         | 0                | 1               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/rechtsstaatsverfahren-gegen-ungarn)
<div style="page-break-after: always;"></div>

## Reform der Dublin-Verordnung

Die Reform der Dublin-Verordnung wurde vom EU-Parlament mit 290 Jastimmen zu 175 Neinstimmen und 44 Enthaltungen angenommen.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Außenpolitik und internationale Beziehungen
- Ausländerpolitik, Zuwanderung

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                | x         |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           |                  | x               |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           |                |           |                  | x               |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 61             | 4         | 8                | 23              |
|                             | AfD                           | 0              | 0         | 0                | 1               |
|                             | CDU                           | 18             | 1         | 1                | 9               |
|                             | CSU                           | 2              | 1         | 0                | 2               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 8              | 0         | 0                | 3               |
|                             | DIE LINKE                     | 3              | 2         | 0                | 2               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 22             | 0         | 0                | 5               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/reform-der-dublin-verordnung)
<div style="page-break-after: always;"></div>

## Reform des Europawahlrechts

Das EU-Parlament hat über die Umverteilung der Sitze des Vereinigten Königreichs nach dem vollzogenen Brexit abgestimmt. Außerdem stand die Einführung einer transnationalen Wahlliste für die Europawahl 2019 zur Debatte. Der Antrag wurde mit einer Mehrheit von 400 Jastimmen zu 183 Gegenstimmen und 96 Enthaltungen angenommen.
	Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Parlamentsangelegenheiten
- Wahlprüfung

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           | x                |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           | x                |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           |                |           | x                |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                |           | x                |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           | x                |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 50             | 4         | 37               | 5               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 22             | 2         | 4                | 1               |
|                             | CSU                           | 1              | 0         | 4                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 0              | 0         | 11               | 0               |
|                             | DIE LINKE                     | 0              | 1         | 5                | 1               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 23             | 1         | 0                | 3               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/reform-des-europawahlrechts)
<div style="page-break-after: always;"></div>

## Resolution zur Neuzulassung des Herbizids Glyphosat

Die EU-Parlamentarier beschlossen mit 374 Stimmen bei 225 Gegenstimmen und 102 Enthaltungen die nichtbindende Resolution&nbsp;zur Neuzulassung des umstrittenen Unkrautvernichtungsmittels Glyphosat. Die Abstimmungsergebnisse der deutschen EU-Abgeordneten finden Sie hier.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Landwirtschaft und Ernährung

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                | x         |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           |                | x         |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           | x                |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                | x         |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           | x                |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
|                             | Total                         | 54             | 16        | 18               | 7               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 24             | 1         | 0                | 4               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 0                | 1               |
|                             | DIE GRÜNEN                    | 0              | 11        | 0                | 0               |
|                             | DIE LINKE                     | 0              | 0         | 6                | 1               |
|                             | FDP                           | 0              | 0         | 3                | 0               |
|                             | SPD                           | 23             | 2         | 0                | 1               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/resolution-zur-neuzulassung-des-herbizids-glyphosat)
<div style="page-break-after: always;"></div>

## Rolle des Deutschen Jugendamts bei grenzüberschreitenden Familienangelegenheiten

Am 29.11.2018 hat das Europäische Parlament einem Entschließungsantrag zur Rolle des Deutschen Jugendamts in grenzüberschreitenden Familien zugestimmt. Der Antrag fordert die Kommission dazu auf, eine aktive Rolle bei der Gewährleistung fairer und diskriminierungsfreier Verfahrensweisen gegenüber Eltern bei der Behandlung grenzüberschreitender Fälle zu spielen. Die Fraktion der Christdemokraten und die Fraktion der Konservativen&nbsp;stimmten mehrheitlich gegen den Antrag. Die deutschen Abgeordneten der sozialdemokratischen S&amp;D-Fraktion stimmten entgegen der Fraktionsmehrheit ebenfalls gegen den Antrag.

- Gesellschaftspolitik, soziale Gruppen
- Familie

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           | x                |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           |                |           | x                |                 |
| Markus Ferber               | CSU                           |                |           | x                |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           |                |           | x                |                 |
| Monika Hohlmeier            | CSU                           |                |           |                  | x               |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           |                  | x               |
| Dr. Angelika Niebler        | CSU                           |                |           | x                |                 |
| Maria Noichl                | SPD                           |                |           | x                |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           |                |           | x                |                 |
| Kerstin Westphal            | SPD                           |                |           | x                |                 |
|                             | Total                         | 9              | 2         | 67               | 18              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 0              | 0         | 23               | 6               |
|                             | CSU                           | 0              | 0         | 4                | 1               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 8              | 0         | 1                | 2               |
|                             | DIE LINKE                     | 0              | 1         | 4                | 2               |
|                             | FDP                           | 0              | 0         | 2                | 1               |
|                             | SPD                           | 0              | 0         | 23               | 4               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/rolle-des-deutschen-jugendamts-bei-grenzueberschreitenden-familienangelegenheiten)
<div style="page-break-after: always;"></div>

## Rückgabe von Beutekunst aus bewaffneten Konflikten und Kriegen

Der Antrag fordert die Kommission auf, Maßnahmen zu ergreifen, die den Kunstmarkt und die Käufer*innen von Artefakten für die Bedeutung von Provenienzforschung (Herkunftsforschung) sensibilisieren.&nbsp;

Die deutschen EU-Abgeordneten stimmten größtenteils für den Antrag. Lediglich die Konservativen stimmten gegen den Antrag. Der Antrag wurde angenommen.&nbsp;

- Kultur
- Forschung

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           |                  | x               |
| Dr. Angelika Niebler        | CSU                           |                |           |                  | x               |
| Maria Noichl                | SPD                           |                |           |                  | x               |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           |                |           |                  | x               |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 75             | 1         | 6                | 14              |
|                             | AfD                           | 0              | 1         | 0                | 0               |
|                             | CDU                           | 26             | 0         | 0                | 3               |
|                             | CSU                           | 3              | 0         | 0                | 2               |
|                             | Die Blauen                    | 1              | 0         | 0                | 0               |
|                             | DIE GRÜNEN                    | 11             | 0         | 0                | 0               |
|                             | DIE LINKE                     | 4              | 0         | 0                | 3               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 23             | 0         | 0                | 4               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/forderungen-nach-rueckgabe-von-beutekunst-aus-bewaffneten-konflikten-und-kriegen)
<div style="page-break-after: always;"></div>

## Schadstoffausstoß mittelgroßer Verbrennungsanlagen begrenzen

Mit einer breiten Mehrheit haben die Abgeordneten im EU-Parlament eine neue Regelung beschlossen, wonach der Schadstoffausstoß von mittelgroßen Verbrennungsanlagen begrenzt werden soll.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Umwelt

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 85             | 0         | 2                | 7               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 26             | 0         | 0                | 3               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 1              | 0         | 0                | 0               |
|                             | DIE GRÜNEN                    | 9              | 0         | 0                | 2               |
|                             | DIE LINKE                     | 6              | 0         | 0                | 1               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 26             | 0         | 0                | 1               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/schadstoffausstoss-mittelgrosser-verbrennungsanlagen-begrenzen)
<div style="page-break-after: always;"></div>

## Schaffung eines Europäischen Struktur- und Investitionsfonds (EFSI)

Das Europäische Parlament hat mit großer Mehrheit seine Zustimmung zum sogenannten Juncker-Fonds gegeben. Der Vorschlag der Kommission sieht über die kommenden drei Jahre Investitionen im Umfang von 315 Milliarden Euro vor und muss abschließend noch vom Ministerrat beschlossen werden.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Öffentliche Finanzen, Steuern und Abgaben
- Finanzen

| Name                    | Partei       | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| ----------------------- | ------------ | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner | ÖDP          | x              |           |                  |                 |
| Albert Deß              | CSU          | x              |           |                  |                 |
| Ismail Ertug            | SPD          |                | x         |                  |                 |
| Markus Ferber           | CSU          | x              |           |                  |                 |
| Thomas Händel           | DIE LINKE    |                |           |                  | x               |
| Monika Hohlmeier        | CSU          | x              |           |                  |                 |
| Barbara Lochbihler      | DIE GRÜNEN   | x              |           |                  |                 |
| Ulrike Müller           | FREIE WÄHLER | x              |           |                  |                 |
| Dr. Angelika Niebler    | CSU          | x              |           |                  |                 |
| Maria Noichl            | SPD          | x              |           |                  |                 |
| Manfred Weber           | CSU          | x              |           |                  |                 |
| Kerstin Westphal        | SPD          | x              |           |                  |                 |
|                         | Total        | 67             | 2         | 8                | 14              |
|                         | AfD          | 0              | 0         | 0                | 1               |
|                         | CDU          | 26             | 0         | 0                | 3               |
|                         | CSU          | 5              | 0         | 0                | 0               |
|                         | Die Blauen   | 0              | 0         | 1                | 0               |
|                         | DIE GRÜNEN   | 6              | 1         | 1                | 3               |
|                         | DIE LINKE    | 0              | 0         | 5                | 2               |
|                         | FDP          | 2              | 0         | 0                | 1               |
|                         | SPD          | 24             | 1         | 0                | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/schaffung-eines-europaeischen-struktur-und-investitionsfonds-efsi)
<div style="page-break-after: always;"></div>

## Schaffung eines wettbewerbfähigen Arbeitsmarkts

Mit knapper Mehrheit hat das EU-Parlament einen Beschluss zur „Schaffung eines von Wettbewerb gekennzeichneten Arbeitsmarkts"&nbsp; angenommen, der als Weg aus der Wirtschafts- und Finanzkrise in Europa und die damit verbundene hohe Arbeitslosigkeit, dienen soll.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Arbeit und Beschäftigung
- Europapolitik und Europäische Union
- Gesellschaftspolitik, soziale Gruppen

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           | x                |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           |                |           | x                |                 |
| Markus Ferber               | CSU                           |                |           |                  | x               |
| Thomas Händel               | DIE LINKE                     |                |           | x                |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                |           | x                |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           |                  | x               |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           |                |           | x                |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           |                |           | x                |                 |
|                             | Total                         | 34             | 3         | 36               | 22              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 21             | 1         | 1                | 6               |
|                             | CSU                           | 4              | 0         | 0                | 1               |
|                             | Die Blauen                    | 1              | 0         | 0                | 0               |
|                             | DIE GRÜNEN                    | 1              | 0         | 7                | 3               |
|                             | DIE LINKE                     | 0              | 0         | 7                | 0               |
|                             | FDP                           | 0              | 2         | 0                | 1               |
|                             | SPD                           | 1              | 0         | 17               | 9               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/schaffung-eines-wettbewerbfaehigen-arbeitsmarkts)
<div style="page-break-after: always;"></div>

## Schutz der akademischen Freiheit 

Am 29. November 2018 stimmten die Abgeordneten des Europaparlaments über eine Empfehlung zum "Schutz der akademischen Freiheit im auswärtigen Handeln der EU" ab. Dadurch werden Rat und Kommission aufgefordert, unionsweite Maßnahmen zum Schutz der Wissenschaftsfreiheit zu treffen.

Die Empfehlung wurde mit 67 Prozent aller Stimmen angenommen. Auch die deutschen Abgeordneten stimmten mehrheitlich zu, darunter vor allem Sozialdemokraten, Abgeordnete der Grüne-Fraktionen und einige Christdemokraten, letztere entschieden mit dieser Meinung allerdings entgegen der restlichen Fraktion. Die deutschen Gegenstimmen kamen aus konservativen sowie aus rechtspopulistischen Fraktionen.

- Bildung und Erziehung
- Menschenrechte
- Wissenschaft, Forschung und Technologie

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                | x         |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                | x         |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           |                |           |                  | x               |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           |                  | x               |
| Dr. Angelika Niebler        | CSU                           |                | x         |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           |                | x         |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 49             | 21        | 9                | 17              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 4              | 14        | 4                | 7               |
|                             | CSU                           | 0              | 3         | 1                | 1               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 9              | 0         | 0                | 2               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 24             | 0         | 0                | 3               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/schutz-der-akademischen-freiheit)
<div style="page-break-after: always;"></div>

## Schutz der finanziellen Interessen der EU 

Am 2. Oktober 2018 hat das Europa-Parlament über einen Entschließungsantrag zum "Schutz der finanziellen Interessen der EU (Einziehung von Finanzmitteln und Vermögenswerten von Drittstaaten in Betrugsfällen)" abgestimmt. Der Antrag forderte mehr Transparenz bei der finanziellen Unterstützung von Drittstaaten und verlangte, dass der Schutz der finanziellen Interessen der EU als zentrales Element in der EU-Politik festgelegt werden sollte, um das Vertrauen der Bürger zu stärken, was die Sicherstellung ihrer Finanzmittel und -abgaben betrifft.

502 MdEP stimmten diesem Antrag zu (81%), somit wurde er angenommen. Von den 96 deutschen Abgeordneten stimmten 75 MdEP dem Antrag zu, hauptsächlich von Seiten der Sozialdemokraten, der Christdemokraten, der Partei der GRÜNEN und der Linken. Die 7 Gegenstimmen und Enthaltungen stammen aus unterschiedlichen Fraktionen wie der ALFA, der AfD, aber auch aus größeren Parteien wie CDU/CSU und SPD.

- Außenpolitik und internationale Beziehungen
- Außenwirtschaft
- Wirtschaft

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           |                  | x               |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 75             | 1         | 6                | 14              |
|                             | AfD                           | 0              | 1         | 0                | 0               |
|                             | CDU                           | 22             | 0         | 0                | 7               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 10             | 0         | 0                | 1               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 25             | 0         | 0                | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/schutz-der-finanziellen-interessen-der-eu)
<div style="page-break-after: always;"></div>

## Schutz geografischer Herkunftsangaben der EU auf nichtlandwirtschaftliche Erzeugnisse

Mit großer Mehrheit haben die Abgeordneten des Europäischen Parlaments die Kommission aufgefordert, den Schutz für geografische Herkunftsangaben auf nichtlandwirtschaftliche Erzeugnisse auszudehnen.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Wirtschaft

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                | x         |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 75             | 9         | 5                | 6               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 25             | 0         | 0                | 4               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 1              | 0         | 0                | 0               |
|                             | DIE GRÜNEN                    | 2              | 8         | 0                | 1               |
|                             | DIE LINKE                     | 7              | 0         | 0                | 0               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 26             | 0         | 0                | 1               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/schutz-geografischer-herkunftsangaben-der-eu-auf-nichtlandwirtschaftliche)
<div style="page-break-after: always;"></div>

## Schutz unbegleiteter minderjähriger Geflüchteter

Die Entschließung des Europäischen Parlaments betont, dass Kinder nicht im Zusammenhang mit Einwanderung festgehalten werden dürfen und ohne Freiheitsentzug unterzubringen sind. Alle Mitgliedsstaaten sind aufgefordert, die in den Übereinkommen der Vereinten Nationen verankerten Rechte von Kindern zu achten.

Mit 343 Ja-Stimmen (53%)&nbsp;wurde die Entschließung angenommen. 287 (44%) Parlamentarier*innen stimmten mit "Nein", während sich 22 (3%) enthielten. Der größte Teil der Christdemokraten und Nationalisten stimmte gegen die Entschließung. Liberale, Grüne und Sozialdemokraten stimmten geschlossen dafür. Von den 96 deutschen EU-Abgeordneten stimmten 50 für die Entschließung, 36 dagegen und 9 waren nicht beteiligt. Es gab eine Enthaltung.



	Das Parlament&nbsp;beauftragt seinen Präsidenten, diese Entschließung dem Rat und der Kommission zu übermitteln.

- Menschenrechte

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           | x                |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           |                |           | x                |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           | x                |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           |                |           | x                |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 49             | 1         | 37               | 9               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 1              | 0         | 26               | 2               |
|                             | CSU                           | 0              | 0         | 5                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 11             | 0         | 0                | 0               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 24             | 0         | 0                | 3               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/schutz-unbegleiteter-minderjaehriger-gefluechteter)
<div style="page-break-after: always;"></div>

## Sicherung des Urheberechts

Das EU-Parlament hat in einer Resolution die Relevanz des Schutzes von Urhebern und Künstlern und ihre schöpferischen Tätigkeiten betont.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Recht

| Name                    | Partei       | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| ----------------------- | ------------ | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner | ÖDP          | x              |           |                  |                 |
| Albert Deß              | CSU          | x              |           |                  |                 |
| Ismail Ertug            | SPD          | x              |           |                  |                 |
| Markus Ferber           | CSU          |                |           |                  | x               |
| Thomas Händel           | DIE LINKE    |                | x         |                  |                 |
| Monika Hohlmeier        | CSU          | x              |           |                  |                 |
| Barbara Lochbihler      | DIE GRÜNEN   | x              |           |                  |                 |
| Ulrike Müller           | FREIE WÄHLER | x              |           |                  |                 |
| Dr. Angelika Niebler    | CSU          | x              |           |                  |                 |
| Maria Noichl            | SPD          | x              |           |                  |                 |
| Manfred Weber           | CSU          | x              |           |                  |                 |
| Kerstin Westphal        | SPD          | x              |           |                  |                 |
|                         | Total        | 61             | 6         | 1                | 22              |
|                         | AfD          | 0              | 0         | 0                | 1               |
|                         | CDU          | 21             | 0         | 0                | 7               |
|                         | CSU          | 4              | 0         | 0                | 1               |
|                         | Die Blauen   | 0              | 0         | 1                | 0               |
|                         | DIE GRÜNEN   | 7              | 0         | 0                | 4               |
|                         | DIE LINKE    | 1              | 6         | 0                | 0               |
|                         | FDP          | 1              | 0         | 0                | 2               |
|                         | SPD          | 22             | 0         | 0                | 5               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/sicherung-des-urheberechts)
<div style="page-break-after: always;"></div>

## Situation von Frauen mit Behinderungen stärken

Der Antrag fordert die Kommission und alle Mitgliedstaaten auf, eine Geschlechter- und Behindertenperspektive in stets in die politische Arbeit&nbsp;mit einzubeziehen. Die deutschen EU-Abgeordneten stimmten mehrheitlich für den Antrag. Gegenstimmen gab es vor allem von den Christdemokraten und den Konservativen.

Der Antrag wurde angenommen.&nbsp;

- Frauen

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           | x                |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           |                |           |                  | x               |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           |                  | x               |
| Dr. Angelika Niebler        | CSU                           |                |           | x                |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           |                |           |                  | x               |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 57             | 2         | 20               | 17              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 11             | 1         | 11               | 6               |
|                             | CSU                           | 1              | 0         | 2                | 2               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 9              | 0         | 0                | 2               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 24             | 0         | 0                | 3               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/situation-von-frauen-mit-behinderungen)
<div style="page-break-after: always;"></div>

## Statut für Sozial- und Solidarunternehmen

Am 05. Juli 2018 hat das Europäische Parlament einen Initiativbericht mit "Empfehlungen an die Kommission zu einem Statut für Sozial- und Solidarunternehmen" vorgelegt, worüber daraufhin namentlich abgestimmt wurde. Mit 398 Zustimmungen wurde der Antrag des Europäischen Parlaments angenommen. Von den 96 deutschen MdEP stimmten 66 dem Initiativbericht zu, 9 stimmten dagegen, einer enthielt sich und 20 MdEP stimmten nicht mit ab. Die Gegenstimmen lassen sich nicht eindeutig zuteilen, da viele dieser Abgeordneten parteilos sind. Nicht mit abgestimmt haben Politiker von Seiten der Christdemokraten, der Sozialdemokraten, der Grünen und der Linken.

- Arbeit und Beschäftigung
- Europapolitik und Europäische Union
- Wirtschaft

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                |           |                  | x               |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 65             | 1         | 9                | 20              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 20             | 0         | 0                | 8               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 7              | 0         | 0                | 4               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 2              | 0         | 0                | 1               |
|                             | SPD                           | 22             | 0         | 0                | 5               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/statut-fuer-sozial-und-solidarunternehmen)
<div style="page-break-after: always;"></div>

## Strengere Transparenzstandards in der EU

Das Europäische Parlament spricht sich für strengere Transparenzstandards in den drei EU-Institutionen aus. Der Bericht über “Transparenz, Rechenschaftspflicht und Integrität in den EU-Institutionen” des Grünen-Abgeordneten Sven Giegold wurde mit einfacher Mehrheit angenommen.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Lobbyismus & Transparenz

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           | x                |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Monika Hohlmeier            | CSU                           |                |           | x                |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           | x                |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           |                  | x               |
| Manfred Weber               | CSU                           |                |           | x                |                 |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 37             | 1         | 29               | 29              |
|                             | AfD                           | 0              | 0         | 0                | 1               |
|                             | CDU                           | 0              | 0         | 24               | 5               |
|                             | CSU                           | 0              | 0         | 5                | 0               |
|                             | Die Blauen                    | 0              | 0         | 0                | 1               |
|                             | DIE GRÜNEN                    | 9              | 0         | 0                | 2               |
|                             | DIE LINKE                     | 3              | 0         | 0                | 4               |
|                             | FDP                           | 2              | 1         | 0                | 0               |
|                             | SPD                           | 14             | 0         | 0                | 13              |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/strengere-transparenzstandards-der-eu)
<div style="page-break-after: always;"></div>

## Stärkung der Rolle von Frauen und Mädchen durch die Digitalwirtschaft

Die Entschließung des Europäischen Parlaments fordert die Mitgliedsstaaten auf, die geschlechtsspezifische Diskrepanz in der Informations- und Kommunikations-Branche zu beheben. Mit 523 Ja-Stimmen (77%)&nbsp;wurde die Entschließung angenommen. 97 (14%) Parlamentarier*innen stimmten mit "Nein", während sich 56 (8%) enthielten.

Von den 96 deutschen EU-Abgeordneten stimmten 76 für die Entschließung, 9 dagegen, 6 waren nicht beteiligt und 5 enthielten sich. Das Parlament&nbsp;beauftragt seinen Präsidenten, diese Entschließung dem Rat und der Kommission zu übermitteln.

- Arbeit und Beschäftigung
- Digitale Agenda
- Menschenrechte

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           |                | x         |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                | x         |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 77             | 4         | 9                | 6               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 24             | 1         | 1                | 3               |
|                             | CSU                           | 2              | 2         | 1                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 10             | 0         | 0                | 1               |
|                             | DIE LINKE                     | 6              | 0         | 0                | 1               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 26             | 0         | 0                | 1               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/staerkung-der-rolle-von-frauen-und-maedchen-durch-die-digitalwirtschaft)
<div style="page-break-after: always;"></div>

## Transparenz und Nachhaltigkeit in der Lebensmittelkette 

Die Entschließung&nbsp;des Europäischen Parlaments begrüßt den Vorschlag für eine Verordnung über die Transparenz und Nachhaltigkeit im Bereich der Lebensmittelkette. Der Entschließungsantrag&nbsp;fordert nun das Parlament in Verhandlungen mit den Rat zu treten, um eine Einigung zu erzielen.

Die Christdemokraten stimmten mehrheitlich gegen den Entwurf. Die Fraktionen Sozialdemokraten, Linke und Grüne stimmten mehrheitlich für den Entwurf. Somit wurde dieser angenommen.



	&nbsp;&nbsp;&nbsp;

- Landwirtschaft und Ernährung

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           |                  | x               |
| Thomas Händel               | DIE LINKE                     | x              |           |                  |                 |
| Nadja Hirsch                | FDP                           |                |           |                  | x               |
| Monika Hohlmeier            | CSU                           |                |           | x                |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                | x         |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           | x                |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           |                |           | x                |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 52             | 2         | 35               | 7               |
|                             | AfD                           | 0              | 1         | 0                | 0               |
|                             | CDU                           | 0              | 0         | 28               | 1               |
|                             | CSU                           | 0              | 0         | 4                | 1               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 9              | 0         | 0                | 2               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 2              | 0         | 0                | 1               |
|                             | SPD                           | 27             | 0         | 0                | 0               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/transparenz-und-nachhaltigkeit-der-lebensmittelkette)
<div style="page-break-after: always;"></div>

## Umsetzung der gemeinsamen Sicherheits- und Verteidigungspolitik

Das Europäische Parlament hat mehrheitlich einem Bericht des Ausschusses für Auswärtige Angelegenheiten zugestimmt.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Verteidigung

| Name                    | Partei       | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| ----------------------- | ------------ | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner | ÖDP          |                |           |                  | x               |
| Albert Deß              | CSU          | x              |           |                  |                 |
| Ismail Ertug            | SPD          |                | x         |                  |                 |
| Markus Ferber           | CSU          |                |           |                  | x               |
| Thomas Händel           | DIE LINKE    |                |           |                  | x               |
| Monika Hohlmeier        | CSU          | x              |           |                  |                 |
| Barbara Lochbihler      | DIE GRÜNEN   |                |           | x                |                 |
| Ulrike Müller           | FREIE WÄHLER | x              |           |                  |                 |
| Dr. Angelika Niebler    | CSU          | x              |           |                  |                 |
| Maria Noichl            | SPD          |                |           | x                |                 |
| Manfred Weber           | CSU          | x              |           |                  |                 |
| Kerstin Westphal        | SPD          |                | x         |                  |                 |
|                         | Total        | 36             | 17        | 25               | 13              |
|                         | AfD          | 0              | 0         | 2                | 0               |
|                         | CDU          | 25             | 0         | 0                | 4               |
|                         | CSU          | 4              | 0         | 0                | 1               |
|                         | DIE GRÜNEN   | 0              | 0         | 9                | 2               |
|                         | DIE LINKE    | 0              | 0         | 6                | 1               |
|                         | FDP          | 2              | 0         | 0                | 1               |
|                         | SPD          | 2              | 17        | 5                | 3               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/umsetzung-der-gemeinsamen-sicherheits-und-verteidigungspolitik)
<div style="page-break-after: always;"></div>

## Umsetzung des Bologna-Prozesses - Steigerung studentischer Auslandsmobilität 

Das Europäische Parlament hat sich mit großer Mehrheit für einen Antrag&nbsp;ausgesprochen, die beschlossenen Reformen des Bologna-Prozesses möglichst schnell umzusetzen.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Bildung und Erziehung
- Europapolitik und Europäische Union

| Name                    | Partei       | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| ----------------------- | ------------ | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner | ÖDP          | x              |           |                  |                 |
| Albert Deß              | CSU          | x              |           |                  |                 |
| Ismail Ertug            | SPD          | x              |           |                  |                 |
| Markus Ferber           | CSU          | x              |           |                  |                 |
| Thomas Händel           | DIE LINKE    |                |           |                  | x               |
| Monika Hohlmeier        | CSU          | x              |           |                  |                 |
| Barbara Lochbihler      | DIE GRÜNEN   | x              |           |                  |                 |
| Ulrike Müller           | FREIE WÄHLER | x              |           |                  |                 |
| Dr. Angelika Niebler    | CSU          | x              |           |                  |                 |
| Maria Noichl            | SPD          | x              |           |                  |                 |
| Manfred Weber           | CSU          | x              |           |                  |                 |
| Kerstin Westphal        | SPD          |                |           |                  | x               |
|                         | Total        | 75             | 4         | 6                | 6               |
|                         | AfD          | 0              | 0         | 2                | 0               |
|                         | CDU          | 28             | 0         | 0                | 1               |
|                         | CSU          | 5              | 0         | 0                | 0               |
|                         | DIE GRÜNEN   | 10             | 0         | 0                | 1               |
|                         | DIE LINKE    | 0              | 4         | 2                | 1               |
|                         | FDP          | 3              | 0         | 0                | 0               |
|                         | SPD          | 25             | 0         | 0                | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/umsetzung-des-bologna-prozesses-steigerung-studentischer-auslandsmobilitaet)
<div style="page-break-after: always;"></div>

## Umsetzung des Handelsabkommens CETA mit Kanada

Das Europäische Parlament hat dem Umfassenden Wirtschafts- und Handelsabkommen (CETA) mehrheitlich zugestimmt. Das stark umstrittene Abkommen wurde mit 408 Ja-Stimmen zu 254 Nein-Stimmen und 33 Enthaltungen angenommen. Vor allem Konservative, Liberale und Sozialdemokraten stimmten für CETA.&nbsp; Zuvor hatten sich 3 Millionen EU-Bürgerinnen und Bürgern in der Petition „Stop TTIP“ gegen das Handelsabkommen ausgesprochen.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Außenwirtschaft
- Europapolitik und Europäische Union

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           | x                |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           |                |           |                  | x               |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                |           | x                |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                | x         |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           |                |           | x                |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 58             | 5         | 28               | 4               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 29             | 0         | 0                | 0               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 0              | 1         | 10               | 0               |
|                             | DIE LINKE                     | 0              | 0         | 6                | 1               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 16             | 3         | 5                | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/umsetzung-des-handelsabkommens-ceta-mit-kanada)
<div style="page-break-after: always;"></div>

## Umsetzung des Siebten Umweltaktionsprogramms

Die Entschließung des Europäischen Parlaments weist darauf hin, dass die EU und ihre Mitgliedsstaaten bei der UN-Klimakonferenz in Paris 2015 das Übereinkommen von Paris unterzeichnet und sich damit verpflichtet haben, seine Ziele zu verwirklichen. Mit 583 Ja-Stimmen (85%)&nbsp;wurde die Entschließung angenommen. 35 (5%) Parlamentarier*innen stimmten mit "Nein", während sich 68 (10%) enthielten.

Von den 96 deutschen EU-Abgeordneten stimmten 84 für die Entschließung, 4 dagegen, 4 waren nicht beteiligt und 4 enthielten sich. Das Parlament&nbsp;beauftragt seinen Präsidenten, diese Entschließung dem Rat und der Kommission zu übermitteln.

- Umwelt
- Naturschutz

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 84             | 4         | 4                | 3               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 25             | 3         | 0                | 1               |
|                             | CSU                           | 4              | 0         | 1                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 9              | 0         | 0                | 1               |
|                             | DIE LINKE                     | 6              | 0         | 0                | 1               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 27             | 0         | 0                | 0               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/umsetzung-des-siebten-umweltaktionsprogramms)
<div style="page-break-after: always;"></div>

## Unterstützung europäischer Investitionsvorhaben im Iran

Der Entschließungsantrag von Jonathan Bullock im Namen der EFDD-Fraktion erhebt Einwände gegen den Delegierten-Beschluss der EU-Kommission, Garantieleistungen für etwaige Verluste zur Unterstützung von Investitionsvorhaben im Iran auszusprechen. Mit 573 Nein-Stimmen wurde der Antrag abgelehnt. 93 Parlamentarier*innen stimmten mit "Ja", während sich 11 enthielten. Christdemokraten, Sozialdemokraten, Liberale, Grüne und Linke stimmten geschlossen gegen den Antrag. Einige Abgeordnete der rechtsextremistischen und nationalistischen Fraktionen stimmten für den Antrag.

Von den 96 deutschen EU-Abgeordneten stimmten 9 für den Entschließungsantrag, 82 dagegen und 5 waren nicht beteiligt. Enthaltungen gab es keine.

- Außenpolitik und internationale Beziehungen
- Außenwirtschaft

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           | x                |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           |                |           | x                |                 |
| Markus Ferber               | CSU                           |                |           | x                |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           |                |           | x                |                 |
| Monika Hohlmeier            | CSU                           |                |           | x                |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                |           | x                |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           | x                |                 |
| Dr. Angelika Niebler        | CSU                           |                |           | x                |                 |
| Maria Noichl                | SPD                           |                |           | x                |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           |                |           | x                |                 |
| Kerstin Westphal            | SPD                           |                |           | x                |                 |
|                             | Total                         | 9              | 0         | 82               | 5               |
|                             | AfD                           | 1              | 0         | 0                | 0               |
|                             | CDU                           | 1              | 0         | 28               | 0               |
|                             | CSU                           | 0              | 0         | 5                | 0               |
|                             | Die Blauen                    | 1              | 0         | 0                | 0               |
|                             | DIE GRÜNEN                    | 0              | 0         | 9                | 2               |
|                             | DIE LINKE                     | 0              | 0         | 5                | 2               |
|                             | FDP                           | 0              | 0         | 3                | 0               |
|                             | SPD                           | 0              | 0         | 27               | 0               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/unterstuetzung-europaeischer-investitionsvorhaben-im-iran)
<div style="page-break-after: always;"></div>

## Unterstützung von Frauen im digitalen Zeitalter

Das EU-Parlament hat einem Bericht mit dem Titel "Gender equality and empowering women in the digital age" zugestimmt. Er hat das Ziel, Sexismus im digitalen Zeitalter verstärkt zu bekämpfen.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Gesellschaftspolitik, soziale Gruppen
- Frauen
- Digitale Agenda

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           |                |           |                  | x               |
| Markus Ferber               | CSU                           |                | x         |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                | x         |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                | x         |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 41             | 11        | 11               | 33              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 8              | 8         | 5                | 8               |
|                             | CSU                           | 3              | 2         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 9              | 0         | 0                | 2               |
|                             | DIE LINKE                     | 3              | 0         | 0                | 4               |
|                             | FDP                           | 0              | 0         | 0                | 3               |
|                             | SPD                           | 13             | 0         | 0                | 14              |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/unterstuetzung-von-frauen-im-digitalen-zeitalter)
<div style="page-break-after: always;"></div>

## Verbesserung der Gleichstellung der Geschlechter 

Am 15.11.2018&nbsp;hat das Europäische Parlament über einen Entschließungsantrag zur&nbsp;Verbesserung der&nbsp;Gleichstellung der Geschlechter abgestimmt. Der Antrag unterstützt den Vorschlag der Kommission für eine Richtlinie zur Vereinbarkeit von Beruf und Privatleben für Eltern und pflegende Angehörige.&nbsp;Mit 385 von 751 Stimmen wurde die Abstimmung angenommen. Die Gegenstimmen kamen größtenteils von den Christdemokraten.&nbsp;

- Arbeit und Beschäftigung
- Frauen

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           | x                |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           |                |           |                  | x               |
| Monika Hohlmeier            | CSU                           |                |           | x                |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           | x                |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 48             | 9         | 18               | 21              |
|                             | AfD                           | 0              | 0         | 0                | 1               |
|                             | CDU                           | 7              | 8         | 8                | 6               |
|                             | CSU                           | 1              | 0         | 4                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 9              | 0         | 0                | 2               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 0              | 0         | 0                | 3               |
|                             | SPD                           | 20             | 0         | 0                | 7               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/verbesserung-der-gleichstellung-der-geschlechter)
<div style="page-break-after: always;"></div>

## Verbesserung des europäischen Trinkwassers

Am 23.10.2018 wurde im Europäischen Parlament über den Vorschlag für eine überarbeitete Richtlinie zur Qualität unseres Trinkwassers abgestimmt. Mit 300 von 751 Ja-Stimmen (45%) wurde die Abstimmung angenommen.
	Von den 96 deutschen Abgeordneten stimmten 38 dafür, 15 dagegen und 39 enthielten sich. Von den 15 Nein-Stimmen gehörten 13 den Christdemokraten an, die anderen ließen sich ebenfalls&nbsp;konservativen Parteien zuordnen. Die Zustimmungen kamen von Seiten der Sozial- und Christdemokraten sowie vereinzelt aus anderen Fraktionen.

- Gesundheit
- Umwelt

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           |                | x         |                  |                 |
| Markus Ferber               | CSU                           |                |           | x                |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           |                | x         |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                | x         |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           | x                |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                | x         |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 38             | 39        | 15               | 4               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 14             | 4         | 10               | 1               |
|                             | CSU                           | 1              | 1         | 3                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 1              | 10        | 0                | 0               |
|                             | DIE LINKE                     | 2              | 4         | 0                | 1               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 13             | 13        | 0                | 1               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/verbesserung-des-europaeischen-trinkwassers)
<div style="page-break-after: always;"></div>

## Verbot von Einwegplastik

Am 24.10.2018 wurde im Europäischen Parlament über ein Gesetz zum Verbot von Einwegkunststoffen abgestimmt. Mit 571 von 751 Ja-Stimmen wurde die Abstimmung angenommen. Von den 96 deutschen Abgeordneten stimmten 78 dafür und 8 dagegen. Die Kontrastimmen kamen hauptsächlich von Seiten der Christdemokraten und vereinzelt aus nationalistischen und rechtsextremistischen Fraktionen.

- Umwelt
- Wissenschaft, Forschung und Technologie

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           |                  | x               |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           |                  | x               |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 78             | 0         | 8                | 10              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 23             | 0         | 2                | 4               |
|                             | CSU                           | 4              | 0         | 1                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 11             | 0         | 0                | 0               |
|                             | DIE LINKE                     | 6              | 0         | 0                | 1               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 25             | 0         | 0                | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/verbot-von-einwegplastik)
<div style="page-break-after: always;"></div>

## Verletzung der Rechte indigener Völker

Die Entschließung des Europäischen Parlaments&nbsp;fordert alle Staaten einschließlich der EU und ihrer Mitgliedsstaaten auf, die territoriale Autonomie und das Recht auf Selbstbestimmung der indigenen Völker rechtlich anzuerkennen und hinzunehmen. Mit 534 Ja-Stimmen (79%)&nbsp;wurde die Entschließung angenommen. 71 (10%) Parlamentarier*innen stimmten mit "Nein", während sich 73 (11%) enthielten. Das Parlament&nbsp;beauftragt seinen Präsidenten, diese Entschließung dem Rat und der Kommission zu übermitteln. Sozialdemokraten, Liberale und GRÜNE stimmten geschlossen mit "Ja". In den anderen europäischen Fraktionen herrschte Uneinigkeit. Besonders häufig mit "Nein" stimmten Abgeordnete der rechtspopulistischen und rechtsextremen Fraktionen.

Von den 96 deutschen EU-Abgeordneten stimmten 77 für die Entschließung, 6 dagegen, 7 waren nicht beteiligt und 6 enthielten sich ihrer Stimme.

- Außenpolitik und internationale Beziehungen
- Menschenrechte

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                | x         |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 77             | 6         | 6                | 6               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 25             | 2         | 1                | 1               |
|                             | CSU                           | 4              | 0         | 1                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 9              | 0         | 0                | 1               |
|                             | DIE LINKE                     | 4              | 0         | 0                | 3               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 27             | 0         | 0                | 0               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/verletzung-der-rechte-indigener-voelker)
<div style="page-break-after: always;"></div>

## Verlängerung der Senkung von Zöllen auf ukrainische Waren 

Vor dem Hintergrund der aktuellen Lage in der Ukraine hat sich das Europäische Parlament dafür ausgesprochen, Zölle auf ukrainische Waren weiterhin zu reduzieren.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Wirtschaft

| Name                    | Partei       | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| ----------------------- | ------------ | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner | ÖDP          | x              |           |                  |                 |
| Albert Deß              | CSU          | x              |           |                  |                 |
| Ismail Ertug            | SPD          | x              |           |                  |                 |
| Markus Ferber           | CSU          | x              |           |                  |                 |
| Thomas Händel           | DIE LINKE    |                |           |                  | x               |
| Monika Hohlmeier        | CSU          | x              |           |                  |                 |
| Barbara Lochbihler      | DIE GRÜNEN   | x              |           |                  |                 |
| Ulrike Müller           | FREIE WÄHLER | x              |           |                  |                 |
| Dr. Angelika Niebler    | CSU          | x              |           |                  |                 |
| Maria Noichl            | SPD          | x              |           |                  |                 |
| Manfred Weber           | CSU          |                |           |                  | x               |
| Kerstin Westphal        | SPD          | x              |           |                  |                 |
|                         | Total        | 69             | 5         | 0                | 17              |
|                         | AfD          | 1              | 1         | 0                | 0               |
|                         | CDU          | 22             | 0         | 0                | 7               |
|                         | CSU          | 4              | 0         | 0                | 1               |
|                         | DIE GRÜNEN   | 9              | 0         | 0                | 2               |
|                         | DIE LINKE    | 0              | 4         | 0                | 3               |
|                         | FDP          | 3              | 0         | 0                | 0               |
|                         | SPD          | 24             | 0         | 0                | 3               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/verlaengerung-der-senkung-von-zoellen-auf-ukrainische-waren)
<div style="page-break-after: always;"></div>

## Verringerung des Lohngefälles zwischen Frauen und Männern

Das Europäische Parlament hat sich mehrheitlich für eine Resolution zum Thema Gleichberechtigung und Gleichbehandlung von Frauen und Männern in Arbeits- und Beschäftigungsfragen&nbsp;ausgesprochen.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Gesellschaftspolitik, soziale Gruppen
- Frauen

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           |                |           |                  | x               |
| Markus Ferber               | CSU                           |                |           | x                |                 |
| Thomas Händel               | DIE LINKE                     | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           |                |           | x                |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                |           |                  | x               |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           | x                |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           |                |           |                  | x               |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 36             | 5         | 26               | 28              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 4              | 4         | 13               | 8               |
|                             | CSU                           | 0              | 0         | 4                | 1               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 4              | 0         | 0                | 7               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 1              | 0         | 2                | 0               |
|                             | SPD                           | 19             | 0         | 0                | 8               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/verringerung-des-lohngefaelles-zwischen-frauen-und-maennern)
<div style="page-break-after: always;"></div>

## Verurteilung der Boko Haram-Angriffe in Nigeria

Nach den jüngsten Angriffen Boko Harams in Nigeria hat das Parlament sich in einer Resolution&nbsp;für eine sofortige Freilassung der im April entführten 276 Schulmädchen sowie für eine strengere Überwachung und Berichterstattung von schweren Kinderrechtsverletzungen ausgesprochen.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Außenpolitik und internationale Beziehungen
- Europapolitik und Europäische Union

| Name                    | Partei       | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| ----------------------- | ------------ | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner | ÖDP          | x              |           |                  |                 |
| Albert Deß              | CSU          | x              |           |                  |                 |
| Ismail Ertug            | SPD          | x              |           |                  |                 |
| Markus Ferber           | CSU          | x              |           |                  |                 |
| Thomas Händel           | DIE LINKE    |                |           |                  | x               |
| Monika Hohlmeier        | CSU          | x              |           |                  |                 |
| Barbara Lochbihler      | DIE GRÜNEN   | x              |           |                  |                 |
| Ulrike Müller           | FREIE WÄHLER | x              |           |                  |                 |
| Dr. Angelika Niebler    | CSU          | x              |           |                  |                 |
| Maria Noichl            | SPD          | x              |           |                  |                 |
| Manfred Weber           | CSU          |                |           |                  | x               |
| Kerstin Westphal        | SPD          | x              |           |                  |                 |
|                         | Total        | 74             | 0         | 0                | 17              |
|                         | AfD          | 1              | 0         | 0                | 1               |
|                         | CDU          | 23             | 0         | 0                | 6               |
|                         | CSU          | 4              | 0         | 0                | 1               |
|                         | DIE GRÜNEN   | 11             | 0         | 0                | 0               |
|                         | DIE LINKE    | 4              | 0         | 0                | 3               |
|                         | FDP          | 3              | 0         | 0                | 0               |
|                         | SPD          | 22             | 0         | 0                | 5               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/verurteilung-der-boko-haram-angriffe-nigeria)
<div style="page-break-after: always;"></div>

## Verurteilung von CIA-Foltermethoden

Das Europäische Parlament hat mit einer Resolution das&nbsp;Inhaftierungs- und Verhörprogramm der CIA verurteilt. Die Abgeordneten begrüßen das von Präsident Obama erlassene Folterverbot.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Außenpolitik und internationale Beziehungen
- Europapolitik und Europäische Union
- Innere Sicherheit

| Name                    | Partei       | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| ----------------------- | ------------ | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner | ÖDP          | x              |           |                  |                 |
| Albert Deß              | CSU          |                |           | x                |                 |
| Ismail Ertug            | SPD          | x              |           |                  |                 |
| Markus Ferber           | CSU          |                |           | x                |                 |
| Thomas Händel           | DIE LINKE    |                |           |                  | x               |
| Monika Hohlmeier        | CSU          |                |           | x                |                 |
| Barbara Lochbihler      | DIE GRÜNEN   | x              |           |                  |                 |
| Ulrike Müller           | FREIE WÄHLER | x              |           |                  |                 |
| Dr. Angelika Niebler    | CSU          |                |           | x                |                 |
| Maria Noichl            | SPD          | x              |           |                  |                 |
| Manfred Weber           | CSU          |                |           | x                |                 |
| Kerstin Westphal        | SPD          | x              |           |                  |                 |
|                         | Total        | 49             | 1         | 36               | 5               |
|                         | AfD          | 2              | 0         | 0                | 0               |
|                         | CDU          | 0              | 0         | 29               | 0               |
|                         | CSU          | 0              | 0         | 5                | 0               |
|                         | DIE GRÜNEN   | 10             | 0         | 0                | 1               |
|                         | DIE LINKE    | 6              | 0         | 0                | 1               |
|                         | FDP          | 1              | 0         | 1                | 1               |
|                         | SPD          | 25             | 0         | 0                | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/verurteilung-von-cia-foltermethoden)
<div style="page-break-after: always;"></div>

## Visa aus humanitären Gründen

Am 14. November 2018 hat das Europäische Parlament über einen Entschließungsantrag mit der Forderung eines rechtlichen Rahmens für Visa aus humanitären Gründen abgestimmt. Hilfsbedürftigen Menschen soll dadurch europaweit internationaler Schutz gewährleistet werden. 65% der MdEP stimmten dem Antrag zu, somit wurde dieser angenommen. Vor allem die Christdemokraten, Sozialdemokraten und die Grünen stimmten dem Antrag zu.

- Außenpolitik und internationale Beziehungen
- Ausländerpolitik, Zuwanderung
- Entwicklungspolitik

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           | x                |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           |                |           |                  | x               |
| Monika Hohlmeier            | CSU                           |                |           | x                |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           | x                |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                | x         |                  |                 |
| Manfred Weber               | CSU                           |                |           | x                |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 44             | 3         | 31               | 18              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 3              | 2         | 19               | 5               |
|                             | CSU                           | 0              | 0         | 5                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 8              | 0         | 0                | 3               |
|                             | DIE LINKE                     | 3              | 0         | 0                | 4               |
|                             | FDP                           | 0              | 0         | 0                | 3               |
|                             | SPD                           | 25             | 0         | 0                | 2               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/visa-aus-humanitaeren-gruenden-0)
<div style="page-break-after: always;"></div>

## Wahl der EU-Kommission

Das Europäische Parlament hat die Zusammensetzung der EU-Kommission unter Präsident Jean-Claude Juncker bestätigt. Das Team um den ehemaligen luxemburgischen Ministerpräsidenten soll am 1. November 2014 die Arbeit aufnehmen.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Parlamentsangelegenheiten

| Name                    | Partei       | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| ----------------------- | ------------ | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner | ÖDP          |                |           | x                |                 |
| Albert Deß              | CSU          | x              |           |                  |                 |
| Ismail Ertug            | SPD          | x              |           |                  |                 |
| Markus Ferber           | CSU          | x              |           |                  |                 |
| Thomas Händel           | DIE LINKE    |                |           |                  | x               |
| Monika Hohlmeier        | CSU          | x              |           |                  |                 |
| Barbara Lochbihler      | DIE GRÜNEN   |                |           | x                |                 |
| Ulrike Müller           | FREIE WÄHLER | x              |           |                  |                 |
| Dr. Angelika Niebler    | CSU          | x              |           |                  |                 |
| Maria Noichl            | SPD          |                |           | x                |                 |
| Manfred Weber           | CSU          | x              |           |                  |                 |
| Kerstin Westphal        | SPD          | x              |           |                  |                 |
|                         | Total        | 56             | 4         | 24               | 7               |
|                         | AfD          | 0              | 0         | 2                | 0               |
|                         | CDU          | 25             | 0         | 1                | 3               |
|                         | CSU          | 5              | 0         | 0                | 0               |
|                         | DIE GRÜNEN   | 0              | 0         | 10               | 1               |
|                         | DIE LINKE    | 0              | 0         | 5                | 2               |
|                         | FDP          | 0              | 3         | 0                | 0               |
|                         | SPD          | 24             | 1         | 2                | 0               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/wahl-der-eu-kommission)
<div style="page-break-after: always;"></div>

## Wahlrechtsreform EU Parlament

Mit seiner Entschließung gibt das Europäische&nbsp;Parlament&nbsp;seine Zustimmung zur Wahlrechtsreform für das&nbsp;Parlament selbst. Mit 397 Ja-Stimmen (60%)&nbsp;wurde die Entschließung angenommen. 207 (31%) Parlamentarier*innen stimmten mit "Nein", während sich 62 (9%) enthielten. Christdemokraten, Sozialdemokraten und einige Liberale stimmten für die Entschließung; alle anderen europäischen Fraktionen dagegen.

Von den 96 deutschen EU-Abgeordneten stimmten 62 für die Entschließung, 28 dagegen und 6 waren nicht beteiligt. Enthaltungen gab es keine.

- Europapolitik und Europäische Union
- Wahlen & Parlamente (Blog)
- Parlamentsangelegenheiten

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           | x                |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                |           | x                |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           | x                |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 62             | 0         | 28               | 6               |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 28             | 0         | 0                | 1               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 2              | 0         | 9                | 0               |
|                             | DIE LINKE                     | 0              | 0         | 5                | 2               |
|                             | FDP                           | 1              | 0         | 0                | 2               |
|                             | SPD                           | 26             | 0         | 0                | 1               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/wahlrechtsreform-eu-parlament)
<div style="page-break-after: always;"></div>

## Weltraumprogramm der Europäischen Union

Zu einer legislativen Entschließung des Europaparlaments über eine Verordnung für das Weltraumprogramm der Union legte die Kommission kürzlich einen überarbeiteten Legislativvorschlag vor, über den am 13.12.2018 abgestimmt wurde. Die schon bestehenden Programme wie Galileo, EGNOS und Copernicus sollen weiterhin gefördert werden, aber auch neue Initiativen sollen entstehen.

Mit 85% wurde der Kommissionsvorschlag im Europäischen Parlament angenommen. Deutsche MdEP's der christdemokratischen und sozialdemokratischen Fraktionen stimmten dem Vorschlag zu, ebenso Abgeordnete der Grüne-Fraktionen. Jörg Meuthen stimmte als einziger deutscher Abgeordneter gegen den Vorschlag.

- Europapolitik und Europäische Union
- Verkehr
- Wissenschaft, Forschung und Technologie

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           |                  | x               |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           |                  | x               |
| Thomas Händel               | DIE LINKE                     | x              |           |                  |                 |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           |                |           |                  | x               |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           |                  | x               |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           |                |           |                  | x               |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 71             | 2         | 1                | 22              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 25             | 0         | 0                | 4               |
|                             | CSU                           | 2              | 0         | 0                | 3               |
|                             | Die Blauen                    | 1              | 0         | 0                | 0               |
|                             | DIE GRÜNEN                    | 10             | 0         | 0                | 1               |
|                             | DIE LINKE                     | 3              | 2         | 0                | 2               |
|                             | FDP                           | 2              | 0         | 0                | 1               |
|                             | SPD                           | 19             | 0         | 0                | 8               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/weltraumprogramm-der-europaeischen-union)
<div style="page-break-after: always;"></div>

## Weltweites Verbot von Tierversuchen

Mit großer Mehrheit hat das Europäische Parlament einen Entschließungsantrag für das weltweite Verbot von Tierversuchen für Kosmetikprodukte angenommen. In ihm wurde die Europäische Kommission dazu aufgefordert, den Vertrieb von zuvor in nichteuropäischen Staaten an Tieren getesteten Kosmetika zu verbieten.

- Außenwirtschaft
- Recht
- Verbraucherschutz

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                |           |                  | x               |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 84             | 1         | 1                | 10              |
|                             | AfD                           | 0              | 0         | 0                | 1               |
|                             | CDU                           | 26             | 1         | 0                | 2               |
|                             | CSU                           | 4              | 0         | 0                | 1               |
|                             | Die Blauen                    | 1              | 0         | 0                | 0               |
|                             | DIE GRÜNEN                    | 11             | 0         | 0                | 0               |
|                             | DIE LINKE                     | 5              | 0         | 0                | 2               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 24             | 0         | 0                | 3               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/weltweites-verbot-von-tierversuchen)
<div style="page-break-after: always;"></div>

## Wie geht es mit der Welthandelsorganisation weiter?

Das Europäische Parlament hat am 29.11.2018 über einen Entschließungsentwurf zur Zukunft der Welthandelsorganisation (WTO) abgestimmt. Der Entwurf sieht vor, dass WTO-Mitgliedsstaaten demokratische Legitimität und Transparenz sicherzustellen haben. Die deutschen EU-Abgeordneten haben mehrheitlich für den Entwurf gestimmt. Der Entwurf wurde angenommen.

- Außenwirtschaft

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           | x                |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           |                |           |                  | x               |
| Barbara Lochbihler          | DIE GRÜNEN                    |                | x         |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           |                  | x               |
| Dr. Angelika Niebler        | CSU                           |                |           | x                |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 62             | 15        | 3                | 16              |
|                             | AfD                           | 1              | 0         | 0                | 0               |
|                             | CDU                           | 23             | 0         | 0                | 6               |
|                             | CSU                           | 3              | 0         | 1                | 1               |
|                             | Die Blauen                    | 1              | 0         | 0                | 0               |
|                             | DIE GRÜNEN                    | 1              | 8         | 0                | 2               |
|                             | DIE LINKE                     | 0              | 5         | 0                | 2               |
|                             | FDP                           | 3              | 0         | 0                | 0               |
|                             | SPD                           | 24             | 0         | 0                | 3               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/wto-wie-geht-es-weiter)
<div style="page-break-after: always;"></div>

## Wiederaufnahme der Friedensbemühungen im Nahen Osten

Das Parlament plädiert mit fraktionsübergreifender Mehrheit für die Wiederaufnahme der Friedensbestrebungen im israelisch-palästinensischen Konflikt.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Außenpolitik und internationale Beziehungen
- Europapolitik und Europäische Union

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           | x              |           |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 82             | 1         | 1                | 11              |
|                             | AfD                           | 1              | 0         | 0                | 0               |
|                             | CDU                           | 27             | 0         | 0                | 2               |
|                             | CSU                           | 5              | 0         | 0                | 0               |
|                             | Die Blauen                    | 1              | 0         | 0                | 0               |
|                             | DIE GRÜNEN                    | 9              | 0         | 0                | 2               |
|                             | DIE LINKE                     | 7              | 0         | 0                | 0               |
|                             | FDP                           | 2              | 0         | 0                | 1               |
|                             | SPD                           | 22             | 1         | 0                | 4               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/wiederaufnahme-der-friedensbemuehungen-im-nahen-osten)
<div style="page-break-after: always;"></div>

## Zugang der Öffentlichkeit zu Dokumenten aus den Jahren 2014-2015

Dem Bericht "über den Zugang der Öffentlichkeit zu Dokumenten [...] für die Jahre 2014–2015" wurde mit einer Mehrheit zugestimmt.&nbsp;

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Recht
- Verbraucherschutz

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                | x         |                  |                 |
| Ismail Ertug                | SPD                           |                |           |                  | x               |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Monika Hohlmeier            | CSU                           |                | x         |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  | x              |           |                  |                 |
| Dr. Angelika Niebler        | CSU                           |                |           | x                |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer | x              |           |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 53             | 4         | 16               | 23              |
|                             | AfD                           | 1              | 0         | 0                | 0               |
|                             | CDU                           | 9              | 2         | 14               | 4               |
|                             | CSU                           | 2              | 2         | 1                | 0               |
|                             | Die Blauen                    | 1              | 0         | 0                | 0               |
|                             | DIE GRÜNEN                    | 9              | 0         | 0                | 2               |
|                             | DIE LINKE                     | 4              | 0         | 0                | 3               |
|                             | FDP                           | 1              | 0         | 0                | 2               |
|                             | SPD                           | 17             | 0         | 0                | 10              |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/zugang-der-oeffentlichkeit-zu-dokumenten-aus-den-jahren-2014-2015)
<div style="page-break-after: always;"></div>

## Zunahme neofaschistischer Gewalttaten in Europa

Am 23.10.2018 wurde im Europäischen Parlament (EP) über einen Entschließungsantrag des EP zur "Zunahme neofaschistischer Gewalttaten in Europa" abgestimmt. Dieser Antrag fordert die Mitgliedstaaten sowie die einzelnen Instrumente der Europäischen Union auf, Maßnahmen gegen die Zunahme von Hassreden, Hetze und daraus resultierende Gewalttaten zu ergreifen und solche strafrechtlich verfolgen zu lassen.

Mit 355 Ja-Stimmen (73%) wurde der Antrag angenommen, woraufhin nun diese Entschließung an den Europäischen Rat, die Europäische Kommission und die Regierungen der Mitgliedstaaten weitergegeben werden soll.



	Von den 96 deutschen Abgeordneten stimmten 42 MdEP dem Antrag zu, wobei diese Zustimmungen vor allem auf die Sozialdemokraten, die Partei der GRÜNEN und der Linken zurückgehen. Die insgesamt 33 Ablehnungen und Enthaltungen kamen von Seiten der Christdemokraten und einige aus rechtspopulistischen und -extremen Parteien.

- Bildung und Erziehung
- Europapolitik und Europäische Union
- Innere Sicherheit

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           | x              |           |                  |                 |
| Albert Deß                  | CSU                           |                |           | x                |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           |                | x         |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           |                  | x               |
| Nadja Hirsch                | FDP                           | x              |           |                  |                 |
| Monika Hohlmeier            | CSU                           |                |           |                  | x               |
| Barbara Lochbihler          | DIE GRÜNEN                    | x              |           |                  |                 |
| Ulrike Müller               | FREIE WÄHLER                  |                |           |                  | x               |
| Dr. Angelika Niebler        | CSU                           |                | x         |                  |                 |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                | x         |                  |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           |                |           |                  | x               |
|                             | Total                         | 42             | 11        | 13               | 30              |
|                             | AfD                           | 0              | 0         | 1                | 0               |
|                             | CDU                           | 5              | 8         | 8                | 8               |
|                             | CSU                           | 1              | 2         | 1                | 1               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 8              | 0         | 0                | 3               |
|                             | DIE LINKE                     | 4              | 0         | 0                | 3               |
|                             | FDP                           | 2              | 0         | 0                | 1               |
|                             | SPD                           | 18             | 0         | 0                | 9               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/zunahme-neofaschistischer-gewalttaten-europa)
<div style="page-break-after: always;"></div>

## Änderung des Wahlrechts zum EU-Parlament

Das&nbsp;EU-Parlament hat einem Bericht zugestimmt,&nbsp;der umfassende Reformen des Wahlsystems für das Europäische Parlament fordert.

Bitte beachten Sie, dass wir nur das Abstimmungsergebnis für die deutschen EU-Abgeordneten darstellen.

- Europapolitik und Europäische Union
- Parlamentsangelegenheiten
- Politisches Leben, Parteien

| Name                        | Partei                        | Dafür gestimmt | Enthalten | Degegen gestimmt | Nicht beteiligt |
| --------------------------- | ----------------------------- | -------------- | --------- | ---------------- | --------------- |
| Prof. Dr. Klaus Buchner     | ÖDP                           |                |           | x                |                 |
| Albert Deß                  | CSU                           | x              |           |                  |                 |
| Ismail Ertug                | SPD                           | x              |           |                  |                 |
| Markus Ferber               | CSU                           | x              |           |                  |                 |
| Thomas Händel               | DIE LINKE                     |                |           | x                |                 |
| Monika Hohlmeier            | CSU                           | x              |           |                  |                 |
| Barbara Lochbihler          | DIE GRÜNEN                    |                |           |                  | x               |
| Ulrike Müller               | FREIE WÄHLER                  |                |           | x                |                 |
| Dr. Angelika Niebler        | CSU                           |                |           |                  | x               |
| Maria Noichl                | SPD                           | x              |           |                  |                 |
| Prof. Dr. Joachim Starbatty | Liberal-Konservative Reformer |                |           | x                |                 |
| Manfred Weber               | CSU                           | x              |           |                  |                 |
| Kerstin Westphal            | SPD                           | x              |           |                  |                 |
|                             | Total                         | 51             | 4         | 24               | 17              |
|                             | AfD                           | 0              | 0         | 0                | 1               |
|                             | CDU                           | 22             | 0         | 2                | 5               |
|                             | CSU                           | 4              | 0         | 0                | 1               |
|                             | Die Blauen                    | 0              | 0         | 1                | 0               |
|                             | DIE GRÜNEN                    | 0              | 4         | 5                | 2               |
|                             | DIE LINKE                     | 0              | 0         | 5                | 2               |
|                             | FDP                           | 1              | 0         | 0                | 2               |
|                             | SPD                           | 24             | 0         | 0                | 3               |

Details: [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/eu/abstimmungen/aenderung-des-wahlrechts-zum-eu-parlament)
<div style="page-break-after: always;"></div>
